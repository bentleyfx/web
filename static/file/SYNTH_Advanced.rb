# SYNTH Advanced
#
# 1. Verändere die Zahlen nach Lust und Laune und getraue dich auch Dinge grosszügig
#    zu löschen. Du kannst immer wieder von vorne beginnen.
#    Die Fehlermeldung ist meistens auf der oder vor der beschriebenen Zeile.
#
# 2. Was noch neu dazukommt, sind zum Beispiel Variabeln und eigene Funktionen.
#    Ebenso ist der Zufall super spannend...
#    Die Änderung der oben definierten Zahlen nach den Variabeln "time", etc... nehmen
#    nur Einfluss auf den Klang, wenn du den Code mit "CMD+S" zuerst stoppst und erst
#    dann wieder abspielst.
#
# 3. Jetzt heisst es nur noch weiter im Tutorial spannende Sachen suchen und
#    in dein Programm hineinkopieren. Das kannst du mit Rechtsklick oder auf dem
#    Touchpad mit zwei Fingern gleichzeitig gedrückt.
#
#
#    Viel Spass :D !!!

time = 0.125
leise = 0.1
lang = 2

live_loop :melodie do
  use_synth :dpulse
  # Die drei Zahlen sind definiert als: stairs, zufall und bling!
  melodie 20, 40, 0.8
end


live_loop :bass do
  ton = rrand_i(30, 50)
  s = play ton , amp: 1, release: 2, note_slide: 1
  sleep choose([1.2,0.6,0.3,0.075])
  control s, note: :G1, amp: rand, cutoff: 0.8
  play [:e3, :g3, :b3, ton], amp: 1, release: 4 if one_in(4)
  sleep time
end


live_loop :ambi do
  20.times do
    with_fx :reverb, room: 1 do
      use_synth :sine
      use_random_seed 50
      notes = (scale :g3, :major_pentatonic).shuffle
      play notes.tick, amp: 0.3, release: 1.3, pan: rand
      sleep choose([time,time/2,time/4])
    end
  end
  sleep 3*time
end












define :melodie do |stairs, zufall, bling|
  if one_in(zufall)
    stairs.times do
      osloton
      sleep time
    end
    with_fx :distortion, pre_amp: 6 do
      sleep bling
      play  :e5, amp: leise
      sleep bling
      play  :b5, amp: leise, release: lang
      sleep bling
      play  :g4, amp: leise
      sleep bling if one_in(2*zufall)
      play  :a5, amp: leise, release: lang
      sleep bling
      play  :c5, amp: leise
      sleep bling
      play  :g4, amp: leise, release: lang
      sleep bling
      play  :b4, amp: leise, release: lang if one_in(zufall)
    end
  else
    with_fx :flanger, depth: 4 do
      riff = (ring :e3, :e2, :r, :g3, :r, :b5, :r, :a3, :b3, :c3, :d3)
      play riff.tick, amp: 1, release: 0.5, cutoff: rrand(80,90), pan: -0.6
    end
    sleep choose([time,2*time])
  end
end

define :osloton do
  with_fx :echo, phase: 4 do
    with_fx :bitcrusher, bits: 8 do
      ton = [:g1,:e2,:a3,:b4,:g4,:g5].ring
      play ton.tick, amp: rand, attack: 0.01, sustain: 0.1, release: 0.1
    end
  end
  sleep time
end