# SYNTH Intermediate
#
# 1. Der erste Loop heisst ":Ring", weil wir darin einen "ring" definieren.
#    Füge noch mehr Nummern dazu, drücke "CMD+R" und geniesse die Ruh!
#
# 2. Wenn man play und die Nummern untereinander ohne sleep schreibt, dann
#    spielen die Töne zur genau gleichen Zeit. Das sind Akkorde!
#    Im zweiten Loop siehst du mehrere Möglichkeiten, wie man das machen kann.
#
# 3. Ähnlich wie Akkorde kann man auch ganze Patterns abspielen.
#    Lösche ":melodic_minor_asc" und schreibe ":" und einen Buchstaben um die
#    Tonleiter zu wechseln. Viel Geduld, es kommt schon gut :D !

live_loop :Ring do
  use_synth :piano
  r = [50,54,57,61,64].ring
  play r.tick, amp: 1
  sleep 0.25
end

live_loop :AkkordeUndPattern do
  play chord(:e4, :m9), release: 1.5
  sleep 0.125
  20.times do
    play [:E3, :A3, :Cs]
    sleep 0.5
  end
  1.times do
    play_pattern_timed scale(:e3, :melodic_minor_asc), 0.125, release: 0.4
    sleep 0.125
    a = [:A2, :Cs4, :E5]
    play a
    sleep 0.25
  end
end