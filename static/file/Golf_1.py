from turtle import *

zeichnung = Screen()

ball = Turtle()
ball.ht()

def fussball():
  ball.fillcolor("black")
  ball.begin_fill()
  ball.circle(10)
  ball.end_fill()

def vor():
    ball.tracer(0)
    
    ball.clear()
    ball.pu()
    ball.fd(4)
    ball.pd()
    
    fussball()
    
    ball.update()

fussball()

zeichnung.listen()
zeichnung.onkey(vor,"Right")
zeichnung.exitonclick()