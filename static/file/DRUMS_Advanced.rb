# DRUMS Advanced
#
# 1. Verändere die Zahlen nach Lust und Laune und getraue dich auch Dinge grosszügig
#    zu löschen. Du kannst immer wieder von vorne beginnen.
#    Die Fehlermeldung ist meistens auf der oder vor der beschriebenen Zeile.
#
# 2. Was noch neu dazukommt, sind zum Beispiel Variabeln und eigene Funktionen.
#    Ebenso ist der Zufall super spannend...
#    Die Änderung der oben definierten Zahlen nach den Variabeln "time", etc... nehmen
#    nur Einfluss auf den Klang, wenn du den Code mit "CMD+S" zuerst stoppst und erst
#    dann wieder abspielst.
#
# 3. Jetzt heisst es nur noch weiter im Tutorial spannende Sachen suchen und
#    in dein Programm hineinkopieren. Das kannst du mit Rechtsklick oder auf dem
#    Touchpad mit zwei Fingern gleichzeitig gedrückt.
#
#
#    Viel Spass :D !!!
  
time = 0.6
laut = 0.3
kurz = 2

live_loop :groove do
  #floor, luck, bling
  groove 36, 8, 0.3
  sleep time
end


live_loop :hihat do
  3.times do
    sample :perc_bell, rate: rrand(0.1,0.03), amp: laut, attack: 0.35, sustain: choose([0.05, 0.04])
    sleep choose([time/8,time/2,time])
    2.times do
      sample :drum_cymbal_open, amp: laut, rate: 4, attack: 0.004, sustain: rrand(0,0.1)
    end
    sleep time/4
  end
  2.times do
    with_fx :gverb, room: 1 do
      oslohit
      sleep time/4
    end
  end
end


live_loop :percussion do
  kurz.times do
    sample :tabla_ghe1, amp: laut if one_in(2)
    sleep 1*time/4
    sample :tabla_ghe1, amp: laut if one_in(3)
    sleep 3*time/4
    (time*kurz).times do
      sample :tabla_ke1, amp: 1
      sleep time/kurz
    end
    (kurz*2).times do
      sample :tabla_tas3, amp: rand
      sleep time/kurz
    end
  end
end












define :groove do |floor, luck, bling|
  floor.times do
    sample :drum_cymbal_open, amp: laut, sustain: 0.04
    sample :bd_tek, start: rrand(0,0.1), finish: rrand(0.5,1) if one_in(6)
    sleep bling
    sleep bling if one_in(4)
    sample :bd_tek, start: rrand(0,0.5), finish: rrand(0.5,1), cutoff: 80, cutoff_slide: 4 if one_in(8)
    if one_in(luck)
      choose([1,2]).times do
        sample :elec_filt_snare, amp: 0.5, rate: 4, attack: 0.001, sustain: 6, release: 0.4 if one_in(2)
        sleep bling/2
        sample :sn_generic, amp: 0.4, rate: rrand(1,1.4)
      end
    end
  end
  (floor/4).times do
    use_synth :dark_ambience
    sample :ambi_haunted_hum, amp: 1, sustain: 1, release: 2
    sleep 4*bling
  end
end
sample

define :oslohit do
  hit = [:elec_plip, :elec_ping, :elec_bell, :elec_cymbal].ring
  sample hit.tick, pitch: choose([10,24,16])
end