# SYNTH Simple
#
# 1. Versuche mal die Zahl nach dem zweiten Play zu verändern.
#    Welche Nummer gibt denselben Ton wie das "C3"?
#
# 2. Kopiere das "play" mit der Zahl zusammen mit dem "sleep" und der Zahl
#    und kopiere sie vor dem end in die Schleife. Versuche die beiden Zahlen
#    nach Belieben zu verändern. Kannst du so eine dir bekannte Melodie programmieren?
#
# 3. Wenn du die Tonhöhe nicht als Nummer schreiben willst,
#    kannst du auch die Noten, wie ":C4", ":D3" schreiben.
#    Ändere jetzt mal den Synthesizerklang in dem du ":tb303" ersetzt.

live_loop :Melodie2 do
    use_synth :tb303
    play :C3
    sleep 2
#  with_fx :tremolo, phase: 0.1 do
    play 80
    sleep 2
#  end
end