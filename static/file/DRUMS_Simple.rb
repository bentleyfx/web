# DRUMS Simple
#
# 1. Versuche mal die Zahl nach "rate" zu verändern
#    und spiele das wieder ab. Was passiert dann genau?
#
# 2. Kannst du so, nur mit dieser Zahl und demselben Sample
#    auch den Klang einer HiHat programmieren?
#    Kopiere alles und bennene den neuen Loop vor dem "do" mit ":HiHat".
#
# 3. Kopiere diesen wieder, bennene den live_loop ":Snare"
#    und ersetze das  ":bd_808" Sample mit einer Snare.
#    Veränderst du die "sleep-Werte" ergibt sich ein Rhythmus :D !

live_loop :BassDrum do
  #  with_fx :compressor, pre_amp: 80 do
  sample :bd_808, amp: 1, rate: 1.2
  sleep 1
  #  end
end