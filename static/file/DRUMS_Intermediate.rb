# DRUMS Intermediate
#
# 1. Im Namen dieses Loops kommt auch "Ring" vor, weil wir darin einen "ring" definieren.
#    Im Ring sind verschiedene Samples definiert. Aus der Hilfe findest du weitere Samples.
#    Kopiere die Namen und füge sie im ring dazu. Drücke "CMD+R" und geniesse den Groove!
#
# 2. Manchmal braucht man eine Pause, damit man merkt, wie toll der Beat klingt.
#    Versuche im gleichen live_loop eine Pause so zu programmieren, dass der Loop
#    wieder im Takt startet!
#
# 3. Mit Verschachtelungen und "n.times do"-Schleifen kannst du auch komplexe Drumbeats machen,
#    die nicht nur auf 4/4-tel basieren. Ändere dafür die Zahl vor dem ".times" und spiele
#    den Loop erneut ab. Crazy :D !

live_loop :VerschachtelungUndRing do
  4.times do
    sample :bd_klub, amp: 1
    3.times do
      sample :elec_triangle, rate: 1.2, finish: 0.02
      sleep 0.25
    end
    sleep 0.025
    sample :bd_klub, amp: 1
    sleep 0.1
    sample :elec_triangle, rate: 1.2, finish: 0.02
    sleep 0.125
    snare = [:elec_hi_snare, :perc_snap, :perc_bell2].ring
    sample snare.tick
    3.times do
      sample :drum_tom_mid_soft, rate: 0.3, sustain: 2, release: 4
      sleep 0.125
    end
    sleep 0.125/2
    sample :elec_triangle, amp: 0.3, rate: 0.3, finish: 0.5
    sleep 0.125/2
  end
end