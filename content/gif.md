![Actioncy_Logo_Schriftzug_sehrklein.gif](/img/Actioncy_Logo_Schriftzug_sehrklein.gif "Ein animiertes Bild mit dem Logo.")
Um GIFs mit dem Computer (oder dem Handy, iPads und Tablets) zu machen gibt es mehrere Möglichkeiten und Programme. Wir haben hier ein paar zusammengestellt, die man auf fast allen Betriebssystemen und Plattformen kostenlos installieren kann und die auch open-Source sind.


# GIFs mit dem Computer/Laptop

# GIMP (Download Workshop-Files)
[GIMP](https://www.gimp.org/ "Link zur Software") ist eine Bildbearbeitungs-Software die auf allen Betriebssytemen läuft.
Mit dem Warp-Tool lassen sich schnell GIFs machen.

Für den GIMP-Workshop mit Laptops findet man alle Files hier:   
[Link zum Actioncy Download-Ordner](https://actioncy.ocloud.de/index.php/s/MDc2sEoQjm7LprD)

{{< embed-pdf url="../file/anleitung_gif.pdf" >}}


# FFmpeg
[FFmpeg](https://ffmpeg.org/) ist ein Tool um Videos und Audio in unterschiedliche Formate zu konventieren. Man kann damit entweder aus Einzelbildern oder Videos auch GIFs machen.
Das Spezielle an FFmpeg ist, dass es eigentlich keine Benutzendenoberfläche gibt. Man arbeitet in der Kommandozeile auf dem Computer. 

Ein einfaches GIF aus Einzelbildern (.PNG-Bilder mit den Namen img1.png, img2.png, img3.png, ..., etc..) ohne transparenten Hintergrund kann man mit nur einer Zeile Code programmieren:
> ffmpeg -f image2 -i img%d.png out.gif  

> Mit "-framerate 4" vor dem "-f" kann man die Geschwindigkeit anpassen. "4" meint hier 4 Bilder in der Sekunde. 


# Stickers mit transparentem Hintergrund auf dem MAC mit FFmpeg im terminal
Für Einzelbilder mit einem transparenten Hintergrund (.PNGs) funktioniert es auf dem Mac so:
1. Öffne das Terminal mit CMD + SPACE und tippe "terminal" und bestätige das Suchresultat mit ENTER.
2. Tippe "cd" (change directory) um den Fokus des Computers in den Ordner mit den Bildern zu wechseln. 
3. Mach einen Abstand!
4. Zieh per Drag and Drop den Ordner in das Terminal.
5. Bestätige mit ENTER!
6. Gib jetzt "ffmpeg -i img%d.png -vf palettegen=reserve_transparent=1 palette.png" ohne die Anführungszeichen ins Terminal ein.
7. Bestätige mit ENTER!
8. Gib "ffmpeg -i img%d.png -i palette.png -lavfi paletteuse -gifflags -offsetting out.gif" im Terminal ein.
9. Bestätige mit ENTER!


# GIFs mit dem iPAD

# RMBG
Um Stickers zu machen, brauchen wir einen transparenten Hintergrund. Das GIF-Format unterstützt Transparenz wie andere Formate auch (beispielsweise .PNG).  
Das open-source Tool [RMBG](https://rmbg.fun/) arbeitet direkt im Browser und benutzt dafür "künstliche Intelligenz". Den Quellcode, das heisst, wie das Programm gebaut wurde ist öffentlich einsehbar und jede Person kann Änderungswünsche anbringen.  
Nach dem Hochladen eines PNGs startet der Prozess automatisch. Danach muss man das Bild herunterladen und für den weiteren Gebrauch mit dem folgenden Tutorial mit "img" + einer Zahl vor dem ".png" benennen.


# FFmpeg online
Mit den iPads kann man leider nicht auf das Terminal zugreifen. Deshalb gibt es dieses [FFmpeg-Online-Tool](https://ffmpeg-online.vercel.app/).

Um die oben beschriebenen Codes für die Sticker nicht von Hand mühsam einzufügen haben wir das in der Internetadresse (URL) schon definiert.

Kurzfassung Generierung der Palette für die Transparenz:
1. Klicke [hier](https://ffmpeg-online.vercel.app/?inputOptions=-i%20img%d.png%20&output=palette.png&outputOptions=-vf%20palettegen=reserve_transparent=1) um auf die besagte URL zu kommen!
2. Lade mehrere Bildersequenzen aus einzelnen .PNGs mit den Namen "img1.png, img2.png, img3.png, ..." hoch.  
> Achtung, die Reihenfolge spielt eine Rolle!
3. Lösche bei Punkt 2. auf der Webseite den zweiten Balken!
4. Klicke run und lade das generierte File ("palette.png") herunter.
5. Das generierte File "palette.png" kopieren wir in den Ordner des Geräts mit den Einzelbildern.

Kurzfassung Generierung des GIFs:
1. Klicke [hier](https://ffmpeg-online.vercel.app/?inputOptions=-framerate%204%20-i%20img%d.png%20&output=out.gif&outputOptions=-i%20palette.png%20-lavfi%20paletteuse%20-gifflags%20-offsetting) für den zweiten Befehl.
2. Lade die Bildersequenzen (.PNGs) erneut hoch. Dieses Mal müssen wir auch das Dokument "palette.png" hochladen.
3. Lösche bei Punkt 2. auf der Webseite den zweiten Balken!
4. Klicke run und lade das generierte GIF herunter. Voilà!


# Urheberrechte
[Hier](https://irights.info/artikel/facebook-und-giphy-bilder-die-bewegen/30106) geht es zum Artikel über die Plattform "GIPHY" von irights.info!  

[Hier](https://irights.info/artikel/urheberecht-im-schulunterricht/32196) geht es um Urheberrechte im Unterricht (irights.info).  

[Hier](https://irights.info/wp-content/uploads/2023/09/31616_Urheberrecht_in_Schulen.pdf) könnt ihr ein PDF von irights.info herunterladen.


# Upload
Ladet eure fertigen GIFs in die [ActioncyGmbH-Cloud](https://actioncy.ocloud.de/index.php/s/oy87ZT2ADxMLSDs)!
