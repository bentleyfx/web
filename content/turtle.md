![dragon.png](/img/dragon.png "Drachenkurve")

# L-Systeme: Struktur 

Ein L-System ist ein Quadrupel G = ( V , S , ω , P ), wobei

    V als Variablen angesehen werden sollen.
    S als Konstanten angesehen werden sollen. Die Zeichen aus V und S bilden das Alphabet des L-Systems.
    ω ein Wort über dem Alphabet ist, welches als Startwort oder Axiom des L-Systems bezeichnet wird.
    P eine Menge von geordneten Paaren aus Wörtern über dem Alphabet ist, welche Ersetzungsregeln definieren. Ist das erste Wort eines jeden Paares ein einzelner Buchstabe aus V, und zu jeder Variablen eine Ersetzungsregel bekannt, so spricht man von einem kontextfreien L-System, sonst von einem kontextsensitiven.

# Anleitung

1. Kopiere den unteren Code in den Online-Editor auf https://trinket.io/.
2. Verändere die Zahlen im Editor, versuche vorher zu erklären, was genau passieren wird und drücke CTR+ENTER!
3. Hast du eine eigene Idee für ein L-System?

> Viel Spass :D !

```python
# Bibliotheken / Module importieren
from turtle import Turtle, Screen, mainloop
from random import *

# Leinwandstil definieren
Leinwand = Screen()
Leinwand.setworldcoordinates(-400,-400,400,400)
Leinwand.tracer(0)

# Das L-system aufsetzen
START = "fx"
REGEL = {'x':'x+yf+', 'y':'-fx-y', 'f':'f', '+':'+', '-':'-'}
STUFE = 15

# Initialisieren einer Schildkröte mit dem Namen "Drago"
Drago = Turtle()
Drago.hideturtle()
Drago.pu()
Drago.goto(100,-220)
Drago.pd()

# Programmiertrick
sub_string = string = START

# Schleife, die sich in der Anzahl der STUFE wiederholt
for _ in range(STUFE):

    # Eine zufällige Farbe für jeden Durchgang
    Drago.pencolor(randint(1,200), randint(1,200), randint(1,200))

    # Anwenden der REGELN und Übersetzen der Zeichen in Schildkrötenbewegung
    for character in sub_string:
        if character == '+':
            Drago.right(90)
        elif character == '-':
            Drago.left(90)
        elif character == 'f':
            Drago.forward(4)

    Leinwand.update()

    # Eine neue Generation von einem L-system
    full_string = "".join(REGEL[character] for character in string)

    sub_string = full_string[len(string):]  # only the new information

    string = full_string  # the complete string for the next generation

mainloop()
```
