---
title: "MEGA – Mobile Electro Guitar Academy"
date: 2019-06-26T15:52:30+02:00
draft: false
---

![](/img/E-Gitarre.jpg)

Ich verstehe meinen Gitarrenunterricht als mit Wissen über Elektronik und Computer erweiterte Musikbildung!

An praktischen Beispielen lernst du neben der Gitarre auch die Grundlagen der elektronischen Musik kennen. 
Und das Beste daran ist, dass das alles bequem bei dir zuhause stattfindet.

Der Unterricht in Luzern, Zug und Zürich kostet CHF 150.- pro volle Stunde 
(andere Städte auf Anfrage). Der Unterricht kann auch zu zweit oder als Gruppe gebucht werden. Dasselbe giltet auch für meine Onlinekurse. Online empfehle ich 45min für CHF 111,10. Interessiert? Dann schreibe mir eine [E-Mail](mailto:felix.baenteli@protonmail.com).

# Persönliche Instrumentenvermietung 
Hast du noch kein Instrument und/oder willst du verschiedene Mikrofone, Verstärker und Effektgeräte ausprobieren?

[![Bildlink zu den zu vermietenden Instrumenten mit einem Verstärker von Haha-Fresh.](/img/Vermietung.jpeg "Bild von Felix Bänteli | Verstärker zum Vermieten")](/vermietung)
Ob für Anlässe das richtige PA oder für den Unterricht das passende Instrument. 
Kein Problem! Ich vermiete secondhand-Gitarren, Verstärker und verschiedene Loop- und Effektgeräte.

Klicke auf das Bild für eine aktuelle Auswahl an Equipment!

# Reparaturen
Kabel mit Wackelkontakten und E-Gitarren kann man wie viele andere Sachen reparieren. 

![Auf diesem Bild sieht man viele elektronische Geräte auf einem Bügelbrett.](/img/Reparatur.jpeg "Bild von Felix Bänteli | Beschreibung: Alte elektronische Geräte wiederbelebt")

Im Verein <a href="https://www.laborluzern.ch">LABOR Luzern</a>
[LABOR Luzern](https://www.laborluzern.ch) in der offenen Werkstatt in Kriens ist jeden 
Mittwoch Abend ab 20:00Uhr geöffnet.

Ich bin da auch Mitglied, ermutige euch zuerst zum Selbermachen und helfe dann aber gerne bei Reparaturen :) .
