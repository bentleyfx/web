
[Download "Drums Simple"](/file/DRUMS_Simple.rb "download")
```ruby
# DRUMS Simple
#
# 1. Versuche mal die Zahl nach "rate" zu verändern
#    und spiele das wieder ab. Was passiert dann genau?
#
# 2. Kannst du so, nur mit dieser Zahl und demselben Sample
#    auch den Klang einer HiHat programmieren?
#    Kopiere alles und bennene den neuen Loop vor dem "do" mit ":HiHat".
#
# 3. Kopiere diesen wieder, bennene den live_loop ":Snare"
#    und ersetze das  ":bd_808" Sample mit einer Snare.
#    Veränderst du die "sleep-Werte" ergibt sich ein Rhythmus :D !

live_loop :BassDrum do
  #  with_fx :compressor, pre_amp: 80 do
  sample :bd_808, amp: 1, rate: 1.2
  sleep 1
  #  end
end
```

[Download "Drums Intermediate"](/file/DRUMS_Intermediate.rb "download")
```ruby
# DRUMS Intermediate
#
# 1. Im Namen dieses Loops kommt auch "Ring" vor, weil wir darin einen "ring" definieren.
#    Im Ring sind verschiedene Samples definiert. Aus der Hilfe findest du weitere Samples.
#    Kopiere die Namen und füge sie im ring dazu. Drücke "CMD+R" und geniesse den Groove!
#
# 2. Manchmal braucht man eine Pause, damit man merkt, wie toll der Beat klingt.
#    Versuche im gleichen live_loop eine Pause so zu programmieren, dass der Loop
#    wieder im Takt startet!
#
# 3. Mit Verschachtelungen und "n.times do"-Schleifen kannst du auch komplexe Drumbeats machen,
#    die nicht nur auf 4/4-tel basieren. Ändere dafür die Zahl vor dem ".times" und spiele
#    den Loop erneut ab. Crazy :D !

live_loop :VerschachtelungUndRing do
  4.times do
    sample :bd_klub, amp: 1
    3.times do
      sample :elec_triangle, rate: 1.2, finish: 0.02
      sleep 0.25
    end
    sleep 0.025
    sample :bd_klub, amp: 1
    sleep 0.1
    sample :elec_triangle, rate: 1.2, finish: 0.02
    sleep 0.125
    snare = [:elec_hi_snare, :perc_snap, :perc_bell2].ring
    sample snare.tick
    3.times do
      sample :drum_tom_mid_soft, rate: 0.3, sustain: 2, release: 4
      sleep 0.125
    end
    sleep 0.125/2
    sample :elec_triangle, amp: 0.3, rate: 0.3, finish: 0.5
    sleep 0.125/2
  end
end
```

[Download "Drums Advanced"](/file/DRUMS_Advanced.rb "download")
```ruby
# DRUMS Advanced
#
# 1. Verändere die Zahlen nach Lust und Laune und getraue dich auch Dinge grosszügig
#    zu löschen. Du kannst immer wieder von vorne beginnen.
#    Die Fehlermeldung ist meistens auf der oder vor der beschriebenen Zeile.
#
# 2. Was noch neu dazukommt, sind zum Beispiel Variabeln und eigene Funktionen.
#    Ebenso ist der Zufall super spannend...
#    Die Änderung der oben definierten Zahlen nach den Variabeln "time", etc... nehmen
#    nur Einfluss auf den Klang, wenn du den Code mit "CMD+S" zuerst stoppst und erst
#    dann wieder abspielst.
#
# 3. Jetzt heisst es nur noch weiter im Tutorial spannende Sachen suchen und
#    in dein Programm hineinkopieren. Das kannst du mit Rechtsklick oder auf dem
#    Touchpad mit zwei Fingern gleichzeitig gedrückt.
#
#
#    Viel Spass :D !!!
  
time = 0.6
laut = 0.3
kurz = 2

live_loop :groove do
  #floor, luck, bling
  groove 36, 8, 0.3
  sleep time
end

live_loop :hihat do
  3.times do
    sample :perc_bell, rate: rrand(0.1,0.03), amp: laut, attack: 0.35, sustain: choose([0.05, 0.04])
    sleep choose([time/8,time/2,time])
    2.times do
      sample :drum_cymbal_open, amp: laut, rate: 4, attack: 0.004, sustain: rrand(0,0.1)
    end
    sleep time/4
  end
  2.times do
    with_fx :gverb, room: 1 do
      oslohit
      sleep time/4
    end
  end
end

live_loop :percussion do
  kurz.times do
    sample :tabla_ghe1, amp: laut if one_in(2)
    sleep 1*time/4
    sample :tabla_ghe1, amp: laut if one_in(3)
    sleep 3*time/4
    (time*kurz).times do
      sample :tabla_ke1, amp: 1
      sleep time/kurz
    end
    (kurz*2).times do
      sample :tabla_tas3, amp: rand
      sleep time/kurz
    end
  end
end

define :groove do |floor, luck, bling|
  floor.times do
    sample :drum_cymbal_open, amp: laut, sustain: 0.04
    sample :bd_tek, start: rrand(0,0.1), finish: rrand(0.5,1) if one_in(6)
    sleep bling
    sleep bling if one_in(4)
    sample :bd_tek, start: rrand(0,0.5), finish: rrand(0.5,1), cutoff: 80, cutoff_slide: 4 if one_in(8)
    if one_in(luck)
      choose([1,2]).times do
        sample :elec_filt_snare, amp: 0.5, rate: 4, attack: 0.001, sustain: 6, release: 0.4 if one_in(2)
        sleep bling/2
        sample :sn_generic, amp: 0.4, rate: rrand(1,1.4)
      end
    end
  end
  (floor/4).times do
    use_synth :dark_ambience
    sample :ambi_haunted_hum, amp: 1, sustain: 1, release: 2
    sleep 4*bling
  end
end
sample

define :oslohit do
  hit = [:elec_plip, :elec_ping, :elec_bell, :elec_cymbal].ring
  sample hit.tick, pitch: choose([10,24,16])
end
```
