---
title: "Sound-Performances"
date: 2019-06-26T15:53:06+02:00
draft: false
---
As a guitarist, with experimental electronic music or as a DJ, I travel all over Switzerland.

Click on some images to see a short video! 

![](/img/GeburripartyDaniela_2018.jpg) 

# Sonic-Pi Livecoding 

A lot of studios in Lucerne opened their door for the public on the 18.11.2023.
In a soundperformance I played guitar and wrote code alternating. Man versus machine and machine versus man.
[![](/img/MrFX@SewingMachine.jpeg)](/img/TrashingAround.mp4)
 
# Super Tardigrade

18.3.2022: An der Museumsnacht in Bern habe ich als E-Gitarre spielendes Bärtierchen im Anzug der Künstlerin [kaascat](https://www.kaascat.ch) die Ausstellung SUPER – Die zweite Schöpfung des [Museums für Kommunikation](https://mfk.ch) gehackt :P .
[![Das Kostüm ist mit UV-leuchtender und reflektierender Farbe besprüht.](/img/SuperTardigrade.jpg)](/img/SuperTardigrade.mp4)

# Teiggifest or Bellparknacht or something with Fusilli

26.6.2021: Another legendary acid-trip night with the faboulus Mysterious F in LABOR Lucerne.
[![Mysterious F and MrFX in tha lab!](/img/DJMrFXnMysteriousF_20210626_230514.jpg)](/img/DJMrFXnMysteriousF_20210626_230514.mp4)

# Voiceover Voiceassistance-Deconstruction Assistenz

19.8.2020: Ich assistierte in einem Forschungsprojekt der HSLU Luzern tüftlerisch und künstlerisch in der Zerlegung
von zur Verfügung gestellten digitalen Voiceassistance-Geräten mit vorher mit meiner circuitgebendeten Kassettenrecordergitarre aufgenommenen Anleitungen.
[![Der Anlass hat im Neubad stattgefunden und hier sieht man hinter mir mit Cap, Instrument, Maske und Handschuhe ihr Logo im Hintergrund.](/img/VoiceOverVoiceAssistanceDekonstructionAssistenz.jpg)](/img/VoiceAssistance2324-3642.mp4)

# 10 Years Hackteria

Am 10 Jahr Jubiläum von [Hackteria](https://www.hackteria.org) am 11. Juli 2020 haben wir uns zu einer experimentellen Jamsession im Walchenturm in Zürich zusammengefunden. Zuvor haben wir an einer Session Piezokristalle selber wachsen lassen und am Konzert mit ihnen musiziert.
[![Ich bin derjenige mit dem gelben Regenmantel vor dem Mischpult links.](/img/10YearsHackteria.jpg)](/img/10YearsHackteria.mp4) 

# ByeBye "Äntli Bier Und Ärger"

Zur letzten Radiosendung von Äntli Bier Und Ärger haben sich Felix Bänteli, [Urban Bieri](https://urbanism.xyz/) und Demian Berger nochmals mit allen Instrumenten in der Zwischennutzung "Bermuda Boutique" neben dem Radio LoRa am 23.11.2019 zur Livesendung zusammengefunden.
[![Man sieht Äntli Bier Und Ärger vor der Kunst in der Bermuda Boutique an der Militärstrasse in Zürich.](/img/AentliBierUndAerger_2020.png)](/img/AentliBierUndAerger_LetzteRadiosendung0001-0434.mp4)

# Resonanz

In der Bibliothek von der Fachhochschule Nordwestschweiz in Muttenz gab ich am 18.10.2019 eine pseudowissenschaftliche Soundperformance zum Thema Resonanz in der Vermittlung.
[![Das Bild zeigt mein Set-up zur Soundperformance in Muttenz.](/img/191018_Soundperformance_Muttenz.jpg "Soundperformance Resonanz FHNW")](/img/Resonanz0001-1647.mp4)

# DJ-Session

15.9.2019: Dies war eine weitere eklektische DJ-Session mit Deejay Murat und Livevisualz vom LABOR Luzern im Allgemeinspace der Ateliergemeinschaft Teiggi.
![Hier sieht man mich in einem Popmusikschutzanzug mit Deejay Murat an den Plattenspielern in der Teiggi in Kriens.](/img/DJSessionMitDJMurat_IMG-20190915-WA0005.jpg) 

# The Three Free Three

Das Experimentierlabor am 23.8.19 im Schaufenster vom Einkausfszentrum LOEB in Bern kam mit einer Röhrenmonitor-, Verstärker- und Kartonkistenwand. Mit Mysterious F und [kaascat](https://kaascat.ch/) performte ich da mit verschiedensten Instrumenten.
[![In einer dreiteiligen Installation repräsentiere ich auf diesem Foto den Sound.](/img/TheFreeThreeThree.JPG "Bild von Michael Sutter | Kurator Kunsthalle Luzern")](/img/TheThreeFreeThree0001-1093.mp4)

# Swiss National Day @ Radio LoRa

Am 1. August 2019 im Radio LoRa in Zürich gaben wir eine Performance mit Mysterious F, kaascat und Deejay Murat an den DJ Sessions 2019. 
![Ihr seht hier ein Bild von MrFX musizierend mit dem Noisio https://noisio.de/, Korg Volca Bass und Zoom Sampletrak.](/img/RadioLoRa_DJSessions.JPG)

# Kinderdisco 

Die Kinderdisco am MN-Fest in der Zwischennutzung N49 mit Mysterious F und kaascat am 22. Juli 2019 hatte für alle zum Mitmachen 3 Piezoinstrumente und 3 Handkameras um freundliches Feedback mit Pingu-VHS-Kassetten. 
[![Die Kinderdisco war ein offener Spielplatz mit interaktiven Sound-, Video- und Hellraumprojektoren-Stationen.](/img/Kinderdisco_MysteriousFKaascatMrFX.JPG)](/img/Kinderdisco0001-1944.mp4)

# With Mysterious F @ LABOR Luzern

Dies war die epischste Soundperformance fürs LABOR Luzern mit Mysterious F am Teiggifest vom 22. September 2018. Für ein Video klicke auf das Bild! 
[![Die Livevisualz wurden von Mysterious F auch direkt an die Audiolautsprecherboxen gehängt.](/img/20180922_Teiggifest.jpeg)](/img/VID-20180923-WA0017.mp4)

# Museumsnacht Zug

Den ersten September 2018 verbrachten Koch, [Schneider](https://www.viscosound.ch/) und Bänteli musizierend an der Museumsnacht im Museum für Urgeschichte in Zug. Schlagzeug, Gitarre und Livesampler waren unser Instrumentarium.
![Mit dem Zoom Sampletrak sample ich Klänge die als Kontrast zu den Pfahlbauerfundstücken im Museum von nicht mehr gebrauchten elektronischen Geräten stammen.](/img/Zug_MuseumFuerUrGeschichte_20180901_201449.jpg)

# With Mysterious F @ B74 

Die spontane Soundperformance mit grossen Lautsprecherboxen und DIY-Instrumenten von Mysterious F und mir für [Urs Gaudenz](https://www.gaudi.ch) am 18. Juni 2018 im B74 Raum für Kunst in Luzern war ein voller Erfolg.
![Das Schattenspiel an der Wand war Teil der improvisierten Visualz.](/img/20180618_B74.jpg "MrFX & Mysterious F im B74")

# Radio LoRa

Im Radio LoRa konnte man mich 13 Jahre lang jeden 4. Samstag im Monat als MC der gleichnahmigen Sendung der Jazzband 
[Äntli Bier Und Ärger](https://www.lora.ch/sendungen/alle-sendungen?mode=1&terms=&list=%C3%83%C2%84ntli+Bier+%26+%C3%83%C2%84rger) hören.
![91119_AentliBierUndAerger_LoRa.jpg](/img/91119_AentliBierUndAerger_LoRa.jpg "Äntli Bier Und Ärger") 
