![1200px-Kaffee_als_zimmerpflanze_fcm.jpg](/img/1200px-Kaffee_als_zimmerpflanze_fcm.jpg "Coffee")

# Task 2: Design in a small team a cooperative-controller for one of the following online games. 

Simple vintage games:
- PONG: https://ponggame.io/
- ASTEROIDS: https://www.retrogames.cz/play_125-Atari7800.php
- PITFALL: https://www.retrogames.cz/play_029-Atari2600.php
- TETRIS: https://tetris.com/play-tetris/
- MS.PAC-MAN: https://www.retrogames.cz/play_076-NES.php

With annoying settings in the beginning or special control:
- MINECRAFT: https://www.crazygames.com/game/minecraft-classic
- PAPERBOY: https://www.retrogames.cz/play_038-NES.php?emulator=js
- STREET FIGHTER II: https://www.retrogames.cz/play_304-SNES.php?emulator=retrocc
- CAR RACER: https://www.retrogames.cz/play_1533-SegaMS.php
- MONKEY ISLAND: https://playclassic.games/games/point-n-click-adventure-dos-games-online/play-secret-monkey-island-online/play/

> Challenge: Use your bodies, everyday objects and the room as part of the controller.
