https://actioncy.ocloud.de/index.php/s/2DadQGf232PY9Js

[Download "Synth Simple"](/file/SYNTH_Simple.rb "download")
```ruby
# SYNTH Simple
#
# 1. Versuche mal die Zahl nach dem zweiten Play zu verändern.
#    Welche Nummer gibt denselben Ton wie das "C3"?
#
# 2. Kopiere das "play" mit der Zahl zusammen mit dem "sleep" und der Zahl
#    und kopiere sie vor dem end in die Schleife. Versuche die beiden Zahlen
#    nach Belieben zu verändern. Kannst du so eine dir bekannte Melodie programmieren?
#
# 3. Wenn du die Tonhöhe nicht als Nummer schreiben willst,
#    kannst du auch die Noten, wie ":C4", ":D3" schreiben.
#    Ändere jetzt mal den Synthesizerklang in dem du ":tb303" ersetzt.

live_loop :Melodie2 do
    use_synth :tb303
    play :C3
    sleep 2
#  with_fx :tremolo, phase: 0.1 do
    play 80
    sleep 2
#  end
end
```

[Download "Synth Intermediate"](/file/SYNTH_Intermediate.rb "download")
```ruby
# SYNTH Intermediate
#
# 1. Der erste Loop heisst ":Ring", weil wir darin einen "ring" definieren.
#    Füge noch mehr Nummern dazu, drücke "CMD+R" und geniesse die Ruh!
#
# 2. Wenn man play und die Nummern untereinander ohne sleep schreibt, dann
#    spielen die Töne zur genau gleichen Zeit. Das sind Akkorde!
#    Im zweiten Loop siehst du mehrere Möglichkeiten, wie man das machen kann.
#
# 3. Ähnlich wie Akkorde kann man auch ganze Patterns abspielen.
#    Lösche ":melodic_minor_asc" und schreibe ":" und einen Buchstaben um die
#    Tonleiter zu wechseln. Viel Geduld, es kommt schon gut :D !

live_loop :Ring do
  use_synth :piano
  r = [50,54,57,61,64].ring
  play r.tick, amp: 1
  sleep 0.25
end

live_loop :AkkordeUndPattern do
  play chord(:e4, :m9), release: 1.5
  sleep 0.125
  20.times do
    play [:E3, :A3, :Cs]
    sleep 0.5
  end
  1.times do
    play_pattern_timed scale(:e3, :melodic_minor_asc), 0.125, release: 0.4
    sleep 0.125
    a = [:A2, :Cs4, :E5]
    play a
    sleep 0.25
  end
end
```

[Download "Synth Advanced"](/file/SYNTH_Advanced.rb "download")
```ruby
# SYNTH Advanced
#
# 1. Verändere die Zahlen nach Lust und Laune und getraue dich auch Dinge grosszügig
#    zu löschen. Du kannst immer wieder von vorne beginnen.
#    Die Fehlermeldung ist meistens auf der oder vor der beschriebenen Zeile.
#
# 2. Was noch neu dazukommt, sind zum Beispiel Variabeln und eigene Funktionen.
#    Ebenso ist der Zufall super spannend...
#    Die Änderung der oben definierten Zahlen nach den Variabeln "time", etc... nehmen
#    nur Einfluss auf den Klang, wenn du den Code mit "CMD+S" zuerst stoppst und erst
#    dann wieder abspielst.
#
# 3. Jetzt heisst es nur noch weiter im Tutorial spannende Sachen suchen und
#    in dein Programm hineinkopieren. Das kannst du mit Rechtsklick oder auf dem
#    Touchpad mit zwei Fingern gleichzeitig gedrückt.
#
#
#    Viel Spass :D !!!

time = 0.125
leise = 0.1
lang = 2

live_loop :melodie do
  use_synth :dpulse
  # Die drei Zahlen sind definiert als: stairs, zufall und bling!
  melodie 20, 40, 0.8
end


live_loop :bass do
  ton = rrand_i(30, 50)
  s = play ton , amp: 1, release: 2, note_slide: 1
  sleep choose([1.2,0.6,0.3,0.075])
  control s, note: :G1, amp: rand, cutoff: 0.8
  play [:e3, :g3, :b3, ton], amp: 1, release: 4 if one_in(4)
  sleep time
end


live_loop :ambi do
  20.times do
    with_fx :reverb, room: 1 do
      use_synth :sine
      use_random_seed 50
      notes = (scale :g3, :major_pentatonic).shuffle
      play notes.tick, amp: 0.3, release: 1.3, pan: rand
      sleep choose([time,time/2,time/4])
    end
  end
  sleep 3*time
end

define :melodie do |stairs, zufall, bling|
  if one_in(zufall)
    stairs.times do
      osloton
      sleep time
    end
    with_fx :distortion, pre_amp: 6 do
      sleep bling
      play  :e5, amp: leise
      sleep bling
      play  :b5, amp: leise, release: lang
      sleep bling
      play  :g4, amp: leise
      sleep bling if one_in(2*zufall)
      play  :a5, amp: leise, release: lang
      sleep bling
      play  :c5, amp: leise
      sleep bling
      play  :g4, amp: leise, release: lang
      sleep bling
      play  :b4, amp: leise, release: lang if one_in(zufall)
    end
  else
    with_fx :flanger, depth: 4 do
      riff = (ring :e3, :e2, :r, :g3, :r, :b5, :r, :a3, :b3, :c3, :d3)
      play riff.tick, amp: 1, release: 0.5, cutoff: rrand(80,90), pan: -0.6
    end
    sleep choose([time,2*time])
  end
end

define :osloton do
  with_fx :echo, phase: 4 do
    with_fx :bitcrusher, bits: 8 do
      ton = [:g1,:e2,:a3,:b4,:g4,:g5].ring
      play ton.tick, amp: rand, attack: 0.01, sustain: 0.1, release: 0.1
    end
  end
  sleep time
end
```
