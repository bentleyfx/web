1. Object aus Karton bauen

2. Zwei elektrisch leitende Pads montieren (Alu-Klebeband, Graphitstift oder mit Schrauben)

3. Pads verbinden mit vorhandenen Rest-Kabeln und Krokoklemmenkabel (Siehe Beispiele auf dem Tisch oben)


Mit MakeyMakey:

1. Ein Pad geht zu "Space" und ein Pad auf "Earth"

2. MakeyMakey mit Computer (Scratch geöffnet) und Computer an Mitschpult und/oder an Bildschirm anschliessen

3. Viel Spass!


Mit Synthesizer:

1. Ein Pad wird mit der Kupferfläche "WIDER" und eines mit dem Pad "STAND" auf der Platine verbunden


> Mehr Ideen findet ihr auf Youtube zum Beispiel hier: 
https://www.youtube.com/watch?v=wkPt9MYqDW0&ab_channel=EricRosenbaum
