![FlappyBird.png](/img/FlappyBird.png "Flappy Bird!")

# Task 1: Build a controller for the online-game Flappybird. 

1. Cut a shape or build an object from cardboard.

2. Make two electrically conductive pads with graphite pencil or aluminium adhesive tape.

3. Connect the pads with crocodile clips.

4. One pad connects to "SPACE" on the board and one pad connects to "EARTH".

5. Connect MakeyMakey with your computer and visit: https://flappybird.io/ 

6. Have fun!

> Challenge: The controller must not be operated with the hands!
