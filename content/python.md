# 1. Python Programmierumgebung offline auf deinem Computer

![python-logo@2x.png](/img/python-logo@2x.png "Python")

Installiere die offizielle Python Programmierumgebung auf deinem Computer!

```python
print("Hello world!")
```

# Anleitung der Aufgabe

1. Gehe auf https://www.python.org/downloads/ und klicke auf den Download für dein Betriebssystem.
2. Öffne aus den heruntergeladenen Programmen "IDLE".
3. Im Menu klickst du auf "File" und dann "New File". Hier schreibst du deinen Code! Probiere es zuerst einmal mit dem Code oben aus.

> Um den Code auszuführen klickst du im Menu auf "Run" und dann "Run Module". Du musst zuerst diesen Code speichern und ihm einen Namen geben. Es ist eine gute Idee dein erstes Programm "HelloWorld" zu nennen :) .

_________________________


# 2. "Hilfe, die Robotertiere kommen!"

![robot.png](/img/robot.png "Roboter")

Mit der Funktion print("") könnt ihr zwischen den Anführungszeichen einen Text schreiben. Wir verwenden hier aber nur einzelne Zeichen. Mit vielen solchen Zeichen können wir vom Computer ein Bild "malen" lassen. 

Was gibt der Code unten ausgeführt in einem Python-Interpreter für ein Bild?

```python
Anzahl = 11
print("Suuuiiiiiiiischhhh!")
print("                   ")
print(29*" ")
print(Anzahl*" "+"     /")
print(13*" "+3*"=")
print(Anzahl*" "+7*".")
print(Anzahl*" "+"0"+5*"-"+"0")
print((Anzahl-1)*" "+9*"_")
print((Anzahl+18)*"o")
print(14*(" "+"|"))
print("""
       Art by Morfina
            __
        .--()°'.'
       '|, . ,'
        !_-(_\

""")
```

# Anleitung der Aufgabe

1. Kopiere den Code und füge ihn in ein neues Dokument im Python-Interpreter IDLE ein.
2. Überlege dir, was es für ein Bild gibt und führe danach den Code aus. 
3. Zeichne dein eigenes Robotertier mit einer Sprechblase. Was sagt dein Tier?

> Für ein Bild auf mehreren Zeilen verwenden wir print() nicht nur mit einem, sondern mit 3 Gänsefüsschen: 
print("""  """). Das Ganze nennt man "ASCII-Kunst". Weitere Tierbeispiele findest du hier: https://www.asciiart.eu/animals!
Viel Spass :D .

________________________

# "Wie sieht deine Nase aus?"

![robot_ascii.png](/img/robot_ascii.png "Roboter ASCII")

In der Zusatzaufgabe auf https://projects.raspberrypi.org/en/projects/about-me konntet ihr schon lernen, wie ihr eine Variable definiert. 

> Eine Variable ist wie ein leeres Gefäss, dem ich einen Namen gebe und etwas hineinlegen kann. Zum Beispiel kann das einen Text sein, oder eine Zahl. In unserem Beispiel ist es ein Text (engl. string). Wie heisst die Variable in unserem Beispiel unten?


```python
#!/bin/python3
print("Salut! Ich nenne mich MrFX. Meine Webseite heisst: www.mrfx.ch.")

Name = input("Wie heisst du?")

Name = str(Name)

print("Freut mich sehr "+Name+"!") 

Nase = input("Wie sieht deine Nase aus?")

Nase = str(Nase)

Kopfantwort = input("""
Siehst du so aus? 

   ^-----^
  | 0   0 |
   \  """+Nase+"""  /
    \___/

Ja oder Nein?""")

if str(Kopfantwort) == "Ja":
  print("Ohh! Du siehst toll aus!")
else:
  Ohr = input("""
Sieht aber lustig aus! 

Welche Form haben deine Ohren?""")
  Ohr = str(Ohr)
  print("""
Yeah, cool!

   """+Ohr+"""-----"""+Ohr+"""
  | 0   0 |
   \  """+Nase+"""  /
    \___/
  
Ciao!
 """)

```

# Aufgabe: 

1. Kopiere den Code und füge ihn in den "IDLE-Interpreter" ein. Überlege dir zuerst, was passiert, bevor du den Code ausführst. 
2. Verändere den Kopf im Code so, dass er ähnlich aussieht wie deine Nachbarin oder dein Nachbar.
3. Spielt das Fragespiel zusammen und versteckt den Code!

______________________

# Ein eigenes Quiz!

> Kannst du den Code aus der letzten Aufgabe so verändern, dass bei der richtigen Antwort auf eine Mathematikfrage der Computer dein Lieblingstier erscheinen lässt? 

```python
Frage = input("""Frage 1) Schreibe die Antwort zur Frage: "Was ist Python?":
a) Eine Programmiersprache
b) Ein Computer
c) Ein Interpreter""")
if str(Frage) == "Eine Programmiersprache":
  print("""
   .:.               
             .::::.             
..         ..::::::''::         
::::..  .::''''':::    ''.      
':::::::'         '.  ..  '.    
 ::::::'            : '::   :   
  :::::     .        : ':'   :  
  :::::    :::       :.     .'. 
 .::::::    ':'     .' '.:::: : 
 ::::::::.         .    ::::: : 
:::::    '':.... ''      '''' : 
':::: .:'              ...'' :  
 ..::.   '.........:::::'   :   
  '':::.   '::'':'':::'   .'    
        '..  ''.....'  ..'      
           ''........''
  """)
else:
  print("""              iWs                                 ,W[
              W@@W.                              g@@[
             i@@@@@s                           g@@@@W
             @@@@@@@W.                       ,W@@@@@@
            ]@@@@@@@@@W.   ,_______.       ,m@@@@@@@@i
           ,@@@@@@@@@@@@W@@@@@@@@@@@@@@mm_g@@@@@@@@@@[
           d@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
          i@@@@@@@A*~~~~~VM@@@@@@@@@@Af~~~~V*@@@@@@@@@i
          @@@@@A~          'M@@@@@@A`         'V@@@@@@b
         d@@@*`              Y@@@@f              V@@@@@.
        i@@A`                 M@@P                 V@@@b
       ,@@A                   '@@                   !@@@.
       W@P                     @[                    '@@W
      d@@            ,         ]!                     ]@@b
     g@@[          ,W@@s       ]       ,W@@s           @@@i
    i@@@[          W@@@@i      ]       W@@@@i          @@@@i
   i@@@@[          @@@@@[      ]       @@@@@[          @@@@@i
  g@@@@@[          @@@@@!      @[      @@@@@[          @@@@@@i
 d@@@@@@@          !@@@P      iAW      !@@@A          ]@@@@@@@i
W@@@@@@@@b          '~~      ,Z Yi      '~~          ,@@@@@@@@@
'*@@@@@@@@s                  Z`  Y.                 ,W@@@@@@@@A
""")
```
_____________________

# "Die Schildkröten über den Bildschirm surfen lassen!"

![turtle_profile.gif](/img/turtle_profile.gif "Schildkröte")

Wir haben zuerst auf trinket.io nur die Zahlen experimentell verändert und geschaut was passiert.
Jetzt wollen wir bewusst mit Schildkröten in Python programmieren. Dafür müssen wir zuerst
die Bibliothek mit allen Funktionen von "turtle" in Python importieren. Das nennt man "Modul" und man macht das so:

```python
from turtle import *
```

# Anleitung der Aufgabe

1. Schreibe den Code oben in den IDLE-Python-Interpreter.
2. Bewege die Schildkröte mit den Befehlen "forward()", backward(), "left(), "right()" so, dass das entstehende Bild ein Gefährt für eure Lieblingstiere wird.
3. Versuche mal mit Hilfe der Referenz auf https://docs.python.org/3/library/turtle.html farbige Linien zu machen und die Strichdicke zu ändern.

> Wenn du nicht weiterkommst, kannst du auch deine Banknachbarn fragen :D . Gutes Gelingen! Hier findest du auch ein sogenanntes "Cheatsheet" zum Nachschauen von Befehlen für Turtle-Graphics: {{< embed-pdf url="../file/Turtle-Graphics-Cheat-Sheet.pdf" >}}
 
_____________________

# "Parameter"

![wald.jpg](/img/wald.jpg "Wald mit Bach!")

So kannst du einen Variablenwert direkt mit der Funktion mitgeben. Das nennt man Parameter.

```python
def Form(Seite):
  begin_fill()
  fillcolor("pink")
  for i in range(3):
    forward(Seite)
    left(120)
  end_fill()
  
Form(100)
```

>  Wenn du also eine Variable in der Klammer nach der Funktion schreibst, definierst du einen Parameter.


# Anleitung der Aufgabe

1. Zeichne verschiedene Formen in eigenen Funktionen mit verschiedenen Parametern.
2. Rufe die neue Funktion innerhalb einer for-Schleife einige Male auf und verschiebe den Stift ohne dass er zeichnet an einen neuen Ort.
3. Bei jedem Durchgang soll sich nun der Wert der Parameter ändern. Kannst du so einen Wald mit bunten Bäumen zeichnen?

______________________

# "Eigenes Projekt"

![GolfGame_Python.png](/img/GolfGame_Python.png "Startbild von einem einfachen Golfspiel gezeichnet mit Turtle-Graphics in Python")

Du startest dein eigenes Projekt am besten mit einer Anleitung auf dieser Webseite. Los geht's :D !

> Für Musik: https://projects.raspberrypi.org/de-DE/projects?software%5B%5D=sonic-pi

> Für Bilder: https://projects.raspberrypi.org/de-DE/projects?software%5B%5D=python

# Anleitung der Aufgabe

1. Besuche einen der Links oben und wähle ein Projekt das dich interessiert. 
2. Löse die Aufgaben schrittweise durch.
3. Verändere das Projekt nach Lust und Laune.
4. Bist du fertig, kannst du das Projekt, einen Screenshot, eine Bilddatei (.jpg oder .png) [hier](https://actioncy.ocloud.de/index.php/s/kdjLZbqdCH4JYLX) hochladen oder den Link in das Textdokument kopieren.

> MrFX lädt es dann auf die Internetseite www.actioncy.ch/computerkunst hoch! 

