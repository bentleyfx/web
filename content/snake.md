```python
from turtle import *

f = Turtle()
Spur = [(0.0, 0.0)]
schritt = 10

def east():
#    tracer(0)
    f.seth(0)
    f.fd(schritt)
    x = f.xcor()
    y = f.ycor()
 #   update()
    if ((x,y) in Spur):
        f.goto(-300,-300)
        print("Game Over!")
    Spur.append((x,y))
  #  print((x,y))


def north():
    tracer(0)
    f.seth(90)
    f.fd(schritt)
    x = round(f.xcor())
    y = round(f.ycor())
    update()
    if ((x,y) in Spur):
        f.goto(-300,-300)
        print("Game Over!")
    Spur.append((x,y))
    print((x,y))


def west():
    tracer(0)
    f.seth(180)
    f.fd(schritt)
    x = round(f.xcor())
    y = round(f.ycor())
    update()
    if ((x,y) in Spur):
        f.goto(-300,-300)
        print("Game Over!")
    Spur.append((x,y))
    print((x,y))


def south():
    tracer(0)
    f.seth(270)
    f.fd(schritt)
    x = round(f.xcor())
    y = round(f.ycor())
    update()
    if ((x,y) in Spur):
        f.goto(-300,-300)
        print("Game Over!")
    Spur.append((x,y))
    print((x,y))
    
listen()
onkeypress(east, "Right")
onkeypress(north, "Up")
onkeypress(west, "Left")
onkeypress(south, "Down")

```
