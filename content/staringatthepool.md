---
title: "Kaascat* & MrFX starring: 'Staring At The Pool' – Almost A(n) (e)Motion Picture"
date: 2023-04-14T15:52:42+02:00
draft: false
---
In a residency at the fabulous [Neubad Lucerne](https://neubad.org) [Kaascat*](https://kaascat.ch) and me stared at the pool. Reflections have been made...
{{< vimeo id="1063187614?" title="StaringAtThePool" >}}  
  
  
# Documentation
![](/img/StaringAtThePool_ktmx.jpeg) Flyer © by Kaascat* '23  

Three days we opend the residency in the rooftop-appartment at Neubad Lucerne where we lived during 2 weeks.  
While I was on and off having workshops and other duties Kaascat* worked on her sculptural paintings with flashlights to create stunning shadow-plays.  
Me, I continued my research on given proportions and frames of mobilephones when I suddendly came up with the conclusion that it have to make the people participate. During the first 2 open days I presented my art as a process. I worked directly in the space and let people interact. The last day I had everything set up and visitors could take a mobilephone and explore the art of Kaascat* with the normal flashlight-app and at the same time, the melody of the soundscape was tracked by their movements. See and hear yourself in the video above!  
![](/img/KlavierKaktus_DSC_0156.JPG) In the beautiful inspiring setup of the appartment was a piano standing at the wall. I could play and play. It was a joy. The rest is history!  
