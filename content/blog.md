---
title: "Blog"
date: 2020-09-13T00:26:25+02:00
showDate: false

---

# WACOM Cintiq "27 und älter
Ein Kondensator ist kaputt. Beim anderen ist es ein Wackelkontakt und evtl. ein falscher Stift?

# GOOGLE PIXEL C - Tablet
Dieses Tablet ist ein hoffnungsloser Fall. Zuerst hat der Touchscreen nicht funktioniert, also um präzise zu sein nur der Touch immer und manchmal bei nicht voller Ladung auch der Screen. Dann habe ich per Voiceassistent ohne visuellen Input den Wecker gestellt. Aber es war die falsche Zeit und so geschah das Drama. Seit dem habe ich das Display erhitzt, abgelöst und wieder zusammengebaut. Nun funktioniert es definitiv nicht mehr. Schade. 

# SONY ICF - 7600D FM/LW/MW/SW Receiver
Dieses Radio hat ein sehr schönes Gehäuse. Es funktioniert aber leider nicht mehr.

# APPLE LaserWriter 4/600 PS
Ja, ein schönes Projekt. Mit AppleTalk. Mit dem RaspberryPi könnte man das softwaremässig simulieren und dann auf Ethernet umwandeln, Mit Ethernet dann per Adapter zu USB habe ich gelesen. Doch warum diesen Aufwand? Seit da steht das Gerät einfach bei mir rum.


# ALESIS airFX / airSYNTH
Das Netzteil ist 9VAC, also "alternate current", was soviel heisst, wie "Wechselstrom". Mit dem richtigen Netzteil hat das Gerät also wieder funktioniert. 
[Topic](https://www.gearslutz.com/board/electronic-music-instruments-and-electronic-music-production/947937-alesis-airfx-problems.html)


# Bosch PSR Bohrmaschine
Die Bohrmaschine konnte ich tatsächlich wieder reparieren, aber Leute ernsthaft? Lohnt sich das?
[Anleitung auf ifixit.com](https://de.ifixit.com/Anleitung/Bosch+PSR+18+LI-2+-+Austausch+Planetengetriebe+mit+Welle+f%C3%BCr+Bohrfutter/132862)



