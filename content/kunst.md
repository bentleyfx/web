---
title: "Design and Art"
date: 2019-06-26T15:52:42+02:00
draft: false
---
My creative work ranges from rough graphic design, crazy flyers, lunatic logos to experimental 3D renderings, weird objects and wild videos.

Click on the image to see more!

[![GIGA](/img/Baertierli.JPG)](../designkunst)

# Film- und Videoprojekte
Video- und Filmprojekte gehören seit dem Beginn meiner gestalterischen kreativen Arbeit wie eine Selbstverständlichkeit 
mit dazu. 

{{< vimeo id="295773310?" title="Robolution" >}}

In meinen Videoprojekten auf [Vimeo](https://vimeo.com/user71901967) findet ihr eine Ansammlung von meinen bewegten und bewegenden Bildexperimenten.

Ob Auftragsarbeit, Kunstwerk oder Freizeitangebot für Jugendliche; ich bin offen für Anfragen zu verschiedensten Film-, Video- und Visualzprojekten.   

# Geräte, Instrumente und andere gehackte Hardware
Ausrangierte elektronische Geräte und Musikinstrumente kann man so modifizieren, dass sie wieder Spass machen! 

Das kann zum Beispiel auch die Reparatur des Gerätes bedeuten. Auf ifixit.com findet man viele praktische Anleitungen.

[![CircuitBending](/img/CircuitBending_Instrument.jpg)](../blog)

Man nennt das kreative Anpassen von elektronischen Schaltkreisen "Circuit Bending" :D .


