---
title: "TeleConsciousness MrFX aka Eftihis Mpantelis"
date: 2024-05-01T15:52:42+02:00
draft: false
---

![](/img/phone_0.jpeg)

This swiss retro design-phone from the early 80ies invites exhibition-visitors to take the handset and listen. Being not sure if it is really working patient listeners will be asked to wait and that they will be connected. But connected to what? 

![](/img/Kid_Phone_b.jpg)

Through the noise of everyday life in the city, the glitches and distractions from communication technologies as such, different languages and different cultural backgrounds our understanding is often broken. We loose focus, forget or don’t know anymore who we are, what is right or what is wrong and what is really important to us, our beloved ones and the world. 

![](/img/Nadja_Phone_b.jpg)

The installation of this phone symbolically shows that institutions like the national bank of greece helps to conserve history and the heritage of families/companies and offers documents about ancestry for research.
With the voice of Chrysa Chouliara the sound shapes into a rhythmical pattern with noise and evolves from the distorted present into a structured musical composition. The phone as an object is a fixed starting point to initialise the will to find out about our very human roots and bring them into conciousness no matter how far away they are and how disconnected we feel.

![](/img/All_Phone_b.jpeg)
