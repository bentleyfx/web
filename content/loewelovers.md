---
title: "Loewe Lovers"
date: 2019-03-06T16:33:06+02:00
draft: false
---
![](/img/30_lionlab-lm.JPG)

For the 150th anniversary of the (ambivalent) sculpture of an injured dying lion (or is it just sleeping?) aka "lionmonument" in Lucerne, Switzerland we took part of a big pixel-wall-installation with the local tinkering- and hacking-association [LABOR Luzern](https://laborluzern.ch/). "We" are [kaascat*](https://kaascat.ch/) and me MrFX. Our contribution to the lion-matrix was a so called Loewens-TV with an animated video from the head-part of the lion our pixel represented. 

[![Screenshot of polygonal lion made with blender.org cc by mrfx 2018](/img/Loewe_Poly.png)](https://player.vimeo.com/video/909545362?)
Screenshot of the polygonal lion animation made with [blender.org](https://www.blender.org/) //
Click on the picture to see the video or watch it below. // cc by MrFX '19  
  
  
# TV-program
{{< vimeo id="909545362?" title="LoeweLoversLucerne" >}}

Our video shows the time shortly before the lion was hit by a lance. The exemplary CCTV-like image is this scene and plays it in a loop.  
  
  
# Installation
In the combination of the whole installation our animation shows at times a glimps in the memory of the lion and then again the part to complete the full picture.
![20_lionlab-c_by_KH.jpg](/img/20_lionlab-c_by_KH.jpg "Collage vom Kopf des Löwen in der Installation | Quelle: Kunsthalle Halle Luzern 2019")  
  
  
  