# Willkommen beim SolarBird-Workshop 
![](/img/USBASP_Programmer.jpg "")

Hier kommt eine Anleitung, wie ihr mit einem USBASP-Programmer ([Link Shop Bastelgarage](https://www.bastelgarage.ch/usbasp-avr-programmer-usb-isp-mit-adapter-stecker?search=usbasp)) euren Vogel umprogrammieren könnt :D :

> Installiert die neuste Arduino-Software auf eurem Computer von hier: https://www.arduino.cc/en/software.

1. Öffnet Arduino und Holt euch den klassischen SolarBird-"main.c"-Code von Christoph und Urban:
- Wählt im Menü "File" 
- Geht auf "New Sketch"
- Wählt den vorgegebenen Template-Code aus und löscht ihn
- Geht in einem Internet-Browser auf https://github.com/schaum/solarBird/blob/master/2016SolarbirdWorkshop/EducationalVersion/main.c
- Klickt auf dieser Seite das Kopieren-Icon "Copy raw File" über dem Code um den ganzen "main.c"-Code zu kopieren
- Zurück in der Arduino Software setzt ihr den kopierten Code ein.

![Solarbird](/img/Solarbird_2018_FotoVonAnnaCholinska.jpeg "Solarbird")

2. Um den richtigen Chip auszuwählen geht ihr wie folgt vor:
- Im Menü "Tools" wählt ihr "Board" und da den "Boards-manager…"
- Im Suchfeld schreibt ihr "ATtiny13" und wählt die Library namens "DIY ATtiny" um dieses Paket zu installieren
- Zurück bei "Tools/Board:/DIY ATtiny/" wählt ihr "ATtiny13"
- Bei "Tools/Programmer: " wählt ihr "DIY ATtiny: USBasp + Upload EEPROM (not for Burn Bootloader)"

![Steckverbindung Solarbird](/img/ProgrammerSteckverbindung.jpeg "Steckverbindung")

3. Jetzt wollen wir den Code raufladen! 
- Steckt euren Programmer wie im Bild auf euren SolarBird
- Geht zum Menüpunkt "Sketch" und wählt "Upload Using Programmer"


Viel Spass!!

