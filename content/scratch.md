1. Besucht den Online-Editor von Scratch: https://scratch.mit.edu/projects/editor

2. Klickt auf "Tutorial"

3. Wähle ein Projekt, schaue das Video und löse die Aufgaben Schritt für Schritt.

Viel Spass!
