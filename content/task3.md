![unicorn.png](/img/unicorn.png "unicorn")

# You can remap your makeymakey!

1. Visit: https://makeymakey.com/pages/remap
2. Follow the instructions and set the "Z" and "X" keys
3. Play ROBOT UNICORN ATTACK: https://www.crazygames.com/game/robot-unicorn-attack-heavy-metal

> Have fun :D !
