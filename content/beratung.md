---
title: "Projektsupport"
date: 2019-06-26T15:52:42+02:00
draft: false
---

[![Hier sieht man zwei Fasnachtsmasken von 2017 vom LABOR Luzern.](/img/LuzernerFasnachtM_Labor.jpg "Bild von LABOR Luzern | Quelle: Fasnacht 2017")](/img/Fasnacht_2017_fx10.mp4)

Plant ihr ein komplexes Design- oder Kunstprojekt und wisst bei technischen und/oder künstlerischen Fragen nicht weiter? 

Ich biete meine Mitarbeit in interaktiven Kunstprojekten jeglicher Art an! 

Alle Handys von den Selfish-Masken des LABOR Luzerns von 2018 sind aus MDF im FabLab Zürich gelasert. 

![Der Prototyp wurde in diesem Fasnachtsprojekt unter Zeitdruck schnell zum finalen Objekt.](/img/Fastnacht2017Handy2.jpg "Foto von Felix Bänteli | Quelle: Fasnacht 2017")

Das Plexiglas wurde genau eingepasst und LEDs an den effektivsten Punkten als Hintergrundbeleuchtung für unsere Portraitsfolien platziert. Der Taster für das ein- und ausschalten ist der Homebutton.


# Idee und Planung

Eine gute Planung ist bei jedem mechatronischen Kunstprojekt wichtig. Wieviel Zeit hat man? Wer kann im Team bei welcher Arbeit noch mithelfen?

[![](/img/Fasnacht_2018.png)](/img/VideoFasnacht_2018.mp4)

Gerne spreche ich mit euch über mechatronische Lösungen und helfe je nach Abmachung bei der Ausführung und Inszenierung.

![](/img/MegafonSoundmodule_2018.jpeg) 

Einzelne Teile habe ich mit blender gestaltet und dann an einem Ultimaker 3D-Drucker ausgedruckt.

![.](/img/Steckmodul_Fasnacht_2018.png "Foto von Felix Bänteli | Quelle: Fasnacht 2018")

# Ausführung

Vor allem das Löten elektronischer Schaltkreise ist eine meiner Leidenschaften :-) !

![Hier sieht man zwei Fasnachtsmasken von 2017 vom LABOR Luzern.](/img/Fastnacht_2019.JPG "Bild von LABOR Luzern | Quelle: Fasnacht 2019")

Eine viertelstündige erste Beratung ist gratis, danach kostet es ohne andere Abmachung CHF 150.- pauschal und jede weitere Stunde CHF 50.-.

![Hier sieht man zwei Fasnachtsmasken von 2017 vom LABOR Luzern.](/img/Pfeil_Fastnacht2019.jpg "Bild von LABOR Luzern | Quelle: Fasnacht 2017")

Schreibt mir am besten für das erste Gespräch eine [E-Mail](mailto:felix.baenteli@protonmail.com) mit kurzer Beschreibung eures Projektes. 
