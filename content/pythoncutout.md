# 13. "Game/Kunst-Titel"

![GameTitle.jpg](/img/GameTitle.jpg "Verschiedene alte Computerspieltitel")

Jedes richtige Kunstwerk hat einen Titel. Wie heisst eures? Wie heisst euer Spiel?

# Anleitung der Aufgabe

1. Wenn ihr ein Spiel in Python mit TurtleGraphics gezeichnet habt, dann könnt ihr In der IDLE-Programmierumgebung (Programm aus Aufgabe 1) den Befehl 
```python
from turtle import *
title("Game-Name")
```
verwenden. Der Titel erscheint dann direkt im Fenster oben.

Mit 

```python
colormode(255)
pencolor(39,200,28)
write("Game-Name", None, "center", font=('Helvetica', 20, 'bold')) 
```
könnt ihr einen Titel direkt mit der Schildkröte schreiben.

2. Auf trinket.io könnt ihr nur mit dem Befehl:
```python
from turtle import *
write("Game-Name", None, "center", font=('Helvetica', 20, 'bold')) 
```
arbeiten. Versucht den Namen zu animieren! Versucht auch mal auch andere Schriften aus! Wie könnt ihr die Schrift verändern?

3. Am schönsten ist eine Typographie, die ihr selber mit einer Schildkröte zeichnet. 
Dafür zeichnet ihr jeden Buchstaben einzeln und ruft sie mit einer eigenen Funktion wieder auf.

---
---
---
---
---
---
---
---

# 12. "Upload für die Webseite" 

Nun arbeiten wir an unseren eigenen kleinen Spielen. Um diese anderen zeigen zu können, brauchen wir einen Ort, wo wir sie als Code oder App(Programm) abspeichern können.

# Anleitung der Aufgabe

1. Arbeitet weiter an euren Spielen.
2. Speichert den Code als Text in einem Texteditor ab. 
3. Speichert das Projekt unter einem sprechenden Namen ab.
4. Macht einen Screenshot.
5. Lädt alles hier hoch: https://actioncy.ocloud.de/index.php/s/Y7m4fdQommL7AwJ

---
---
---
---
---
---
---
---

# 11. "Wenn, dann..." 

Eine Bedingung schreibt man in den meisten Programmiersprachen als "if". Hier kommt ein Beispiel:

```python
if i >= 100:
      print("ho")
```   

# Anleitung der Aufgabe
1. Bewege eine Figur mit einer Schildkröte. 
2. Nach einer bestimmten X-Koordinate soll etwas mit "print()" geschrieben werden.

> Schaue auch in den alten Aufgaben nach, wo wir das schon gebraucht haben. 

---
---
---
---
---
---
---
---

# 10. "Eine Funktion mit einer Taste aufrufen!"

Sieh dir die Funktion unten an und versuche mit einer eigenen Funktion die Figur aus Aufgabe 8 bei Tastendruck zu bewegen.

[Ballcode in neuem Tab anzeigen!](/file/Golf_1.py "Code anzeigen!")
```python
from turtle import *

zeichnung = Screen()

ball = Turtle()
ball.ht()

def fussball():
  ball.fillcolor("black")
  ball.begin_fill()
  ball.circle(10)
  ball.end_fill()

def vor():
    ball.tracer(0)
    
    ball.clear()
    ball.pu()
    ball.fd(4)
    ball.pd()
    
    fussball()
    
    ball.update()

fussball()

zeichnung.listen()
zeichnung.onkey(vor,"Right")
zeichnung.exitonclick()
```   

> "zeichnung" oben ist keine Turtle sondern eine Variable der das Objekt "Screen()" zugewiesen wird. Dieses ist wie ein Blatt Papier auf dem wir zeichnen und wir brauchen es, um zu "hören", was wir auf dem Keyboard drücken.  

---
---
---
---
---
---
---
---

# 9. "Die Zeichnung bewegen"

Um die Zeichnung aus der letzten Aufgabe zu bewegen, brauchst du 3 neue Befehle.

1. tracer(0): Damit alles auf einmal gezeichnet wird.
2. clear(): Damit das letzte Bild wie bei einem Daumenkino wieder verschwindet, respektive gelöscht wird.
3. update(): Damit die neue Zeichnung wieder gezeichnet wird. 

> Probiere es aus und bewege deine Figur ein paarmal um "einen Pixel" nach rechts in einer for-Schleife.

---
---
---
---
---
---
---
---

# 8. "Mehrere Zeichnungen gleichzeitig machen!" 

![Westpark_Muenchen_Schildkroeten_in_der_Sonne.jpg](/img/Westpark_Muenchen_Schildkroeten_in_der_Sonne.jpg "2 Schildkröten")

Wollen wir 2 Schildkröten zeichnen nutzen wir die Möglichkeiten der objektorientierten Programmierung.

```python
rasen = Turtle()
rasen.ht()
rasen.begin_fill()
rasen.fillcolor("green")
rasen.circle(100)
rasen.end_fill()

figur = Turtle()
```

# Anleitung der Aufgabe
1. Zeichne einen einfachen Hintergrund mit einer Turtle wie im Beispiel oben.
2. Du kannst nun im Vordergrund eine Figur oder einen einfachen Ball zeichnen. Mach das mal! 
3. Definiere aus der Zeichnung im Vordergrund eine eigene Funktion. Die Funktion muss anders heissen als die Turtle selber. 

> Du weisst sicher, wie das alles geht, oder :) ? Vergiss nicht, die Funktion auch wieder aufzurufen. 

---
---
---
---
---
---
---
---

# 7. "Parameter"

![wald.jpg](/img/wald.jpg "Wald mit Bach!")

So kannst du einen Variablenwert direkt mit der Funktion mitgeben. Das nennt man Parameter.

```python
def Form(Seite,Winkel):
  begin_fill()
  fillcolor("pink")
  for i in range(3):
    forward(Seite)
    left(Winkel)
  end_fill()
  
Form(100,120)
```

>  Wenn du also eine Variable anstatt in der Funktion direkt in der Klammer nach der Funktion schreibst, definierst du einen Parameter.


# Anleitung der Aufgabe

1. Zeichne viele Formen mit verschiedenen Parametern.
2. Rufe die neue Funktion innerhalb einer for-Schleife einige Male auf und verschiebe den Stift ohne dass er zeichnet an einen neuen Ort.
3. Bei jedem Durchgang soll sich nun der Wert der Parameter ändern. Kannst du so einen Wald mit bunten Bäumen zeichnen?

---
---
---
---
---
---
---
---

# 6. "Eine eigene Funktion mit einer Variable"

![baum.jpg](/img/baum.jpg "Wald mit Bach!")

Du hast schon gelernt wie man aus einer einfachen Turtle-Zeichnung eine eigene Funktion definiert. Weisst du noch, wie das geht? Hier kommt ein Beispiel:

```python
# So importierst du das ganze Turtle-Graphics-Modul:
from turtle import *

# Nachstehend schreibe ich "def" und gebe der neuen Funktion den Namen "Tanne".
def Tanne():

# Die Funktion um etwas mit Turtle-Graphics auszumalen heisst "begin_fill". 
  begin_fill()

# Hier wählst du die Farbe. Welche Füllfarbe hat diese Zeichnung?
  fillcolor(50,255,5)

# Mit einer for-Schleife kann ich die selben Zeichnungsanleitungen beliebig oft wiederholen.
  for i in range(3):

# Wichtig ist es, dass ihr bei allem, was ihr in der Schleife machen wollt den Abstand nicht vergesst.
    forward(100)
    left(120)

# Hier ist gibt es wieder nur einen Abstand, da es zur Funktion "Tanne" gehört.
  end_fill()
  
# Ohne diesen Aufruf passiert gar nichts, da wir ja oben nur eine Definition der neuen Funktion gemacht haben.
Tanne()
```


# Anleitung der Aufgabe

1. Zeichne mit Turtle-Graphics in Python ein Rechteck im Python-Interpreter auf https://trinket.io.
2. Ersetze die Zahl in der Funktion "forward" mit dem Wort "Groesse" und schreibe gerade nach dem Importieren des Turtle-Modules welchen Wert die neue Variable "Groesse" haben soll. 
Tipp: Schaue in den anderen Aufgaben auf dieser Seite nach, wie das gehen könnte. 
3. Definiere nun aus dieser Zeichnung eine eigene Funktion, gebe ihr einen eigenen Namen und rufe diese wie im Beispiel oben auf.

> Wenn du nicht weiterkommst, kannst du auch deine Banknachbarn fragen :D . Gutes Gelingen!

---
---
---
---
---
---
---
---

# 5. "Die Schildkröten über den Bildschirm surfen lassen!"

![turtle_profile.gif](/img/turtle_profile.gif "Schildkröte")

Wir haben zuerst auf trinket.io nur die Zahlen experimentell verändert und geschaut was passiert. 

Jetzt wollen wir bewusst mit Schildkröten in Python programmieren. Dafür müssen wir zuerst die Bibliothek mit allen Funktionen von "turtle" in Python importieren. Das nennt man "Modul" und man macht das so:

```python
from turtle import *
```

# Anleitung der Aufgabe

1. Schreibe den Code oben in den Interpreter auf https://trinket.io.
2. Bewege die Schildkröte mit den Befehlen "forward()", backward(), "left(), "right()" so, dass das entstehende Bild ein Gefährt für eure Lieblingstiere wird.  
3. Versuche mal mit Hilfe der Referenz auf https://docs.python.org/3/library/turtle.html farbige Linien zu machen und die Strichdicke zu ändern. 

> Wenn du nicht weiterkommst, kannst du auch auf dem ausgedruckten "Cheatsheet" schauen ;) . 

---
---
---
---
---
---
---
---

# 4. Ein eigenes Quiz!

> Kannst du den Code aus der letzten Aufgabe so verändern, dass bei der richtigen Antwort auf eine Mathematikfrage der Computer dein Lieblingstier erscheinen lässt? 

```python
Frage = input("""Frage 1) Schreibe die Antwort zur Frage: "Was ist Python?":
a) Eine Programmiersprache
b) Ein Computer
c) Ein Interpreter""")
if str(Frage) == "Eine Programmiersprache":
  print("""
   .:.               
             .::::.             
..         ..::::::''::         
::::..  .::''''':::    ''.      
':::::::'         '.  ..  '.    
 ::::::'            : '::   :   
  :::::     .        : ':'   :  
  :::::    :::       :.     .'. 
 .::::::    ':'     .' '.:::: : 
 ::::::::.         .    ::::: : 
:::::    '':.... ''      '''' : 
':::: .:'              ...'' :  
 ..::.   '.........:::::'   :   
  '':::.   '::'':'':::'   .'    
        '..  ''.....'  ..'      
           ''........''
  """)
else:
  print("""              iWs                                 ,W[
              W@@W.                              g@@[
             i@@@@@s                           g@@@@W
             @@@@@@@W.                       ,W@@@@@@
            ]@@@@@@@@@W.   ,_______.       ,m@@@@@@@@i
           ,@@@@@@@@@@@@W@@@@@@@@@@@@@@mm_g@@@@@@@@@@[
           d@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
          i@@@@@@@A*~~~~~VM@@@@@@@@@@Af~~~~V*@@@@@@@@@i
          @@@@@A~          'M@@@@@@A`         'V@@@@@@b
         d@@@*`              Y@@@@f              V@@@@@.
        i@@A`                 M@@P                 V@@@b
       ,@@A                   '@@                   !@@@.
       W@P                     @[                    '@@W
      d@@            ,         ]!                     ]@@b
     g@@[          ,W@@s       ]       ,W@@s           @@@i
    i@@@[          W@@@@i      ]       W@@@@i          @@@@i
   i@@@@[          @@@@@[      ]       @@@@@[          @@@@@i
  g@@@@@[          @@@@@!      @[      @@@@@[          @@@@@@i
 d@@@@@@@          !@@@P      iAW      !@@@A          ]@@@@@@@i
W@@@@@@@@b          '~~      ,Z Yi      '~~          ,@@@@@@@@@
'*@@@@@@@@s                  Z`  Y.                 ,W@@@@@@@@A
  'M@@@*f**W.              ,Z     Vs               ,W*~~~M@@@f
    'M@    'Vs.          ,z~       'N_           ,Z~     d@P
   M@@@       ~\-__  __z/` ,gmW@@mm_ '+e_.   __=/`      ,@@@@
    'VMW                  g@@@@@@@@@W     ~~~          ,WAf
       ~N.                @@@@@@@@@@@!                ,Z`
         V.               !M@@@@@@@@f                gf-
          'N.               '~***f~                ,Z`
            Vc.                                  _Zf
              ~e_                             ,gY~
                'V=_          -@@D         ,gY~ '
                    ~\=__.           ,__z=~`
                         '~~~*==Y*f~~~""")
```

---
---
---
---
---
---
---
---

# 3. "Wie sieht deine Nase aus?"

![robot_ascii.png](/img/robot_ascii.png "Roboter ASCII")

In der Zusatzaufgabe auf https://projects.raspberrypi.org/en/projects/about-me konntet ihr schon lernen, wie ihr eine Variable definiert. 

> Eine Variable ist wie ein leeres Gefäss, dem ich einen Namen gebe und etwas hineinlegen kann. Zum Beispiel kann das einen Text sein, oder eine Zahl. In unserem Beispiel ist es ein Text (engl. string). Wie heisst die Variable in unserem Beispiel unten?


```python
#!/bin/python3
print('Salut! Ich nenne mich MrFX. Meine Webseite heisst: "mrfx.ch".')
Name = input('''
Wie heisst du?''')
Name = str(Name)
print('''
Freut mich sehr '''+Name+'!') 
Nase = input('''
Wie sieht deine Nase aus?''')
Nase = str(Nase)
Kopf = input('''
Siehst du so aus? 

   ^-----^
  | 0   0 |
   \  '''+Nase+'''  /
    \___/

Ja oder Nein?''')
if str(Kopf) == "Ja":
  print('''
Ohh! Du siehst toll aus!''')
else:
  Ohr = input('''
Sieht aber lustig aus! 

Welche Form haben deine Ohren?''')
  Ohr = str(Ohr)
  print('''
Yeah, cool!

   '''+Ohr+'''-----'''+Ohr+'''
  | 0   0 |
   \  '''+Nase+'''  /
    \___/
  
Ciao!
 ''')

```

# Aufgabe: 

1. Kopiere den Code und füge ihn in den Interpreter auf https://trinket.io ein. Überlege dir zuerst, was passiert, bevor du den Code ausführst. 
2. Verändere den Kopf im Code so, dass er ähnlich aussieht wie deine Nachbarin oder dein Nachbar.
3. Spielt das Fragespiel zusammen und versteckt den Code! 

---
---
---
---
---
---
---
---

# 2. "Hilfe, die Robotertiere kommen!"

![robot.png](/img/robot.png "Roboter")

Was gibt der Code unten ausgeführt in einem Python-Interpreter für ein Bild?

```python
Anzahl = 11
print("Suuuiiiiiiiischhhh!")
print("                   ")
print(29*" ")
print(Anzahl*" "+"     /")
print(13*" "+3*"=")
print(Anzahl*" "+7*".")
print(Anzahl*" "+"0"+5*"-"+"0")
print((Anzahl-1)*" "+9*"_")
print((Anzahl+18)*"o")
print(14*(" "+"|"))
```

# Anleitung der Aufgabe

1. Kopiere den Code und füge ihn in den Interpreter auf https://trinket.io ein.
2. Überlege dir, was es für ein Bild gibt und führe danach den Code aus. 
3. Verändere den Code so, dass daraus dein individuelles Robotertier wird. Wie sieht das aus? Verwende dafür die Zeichen aus dem Beispielcode und ein weiteres, das dir gefällt.

> Mit der Funktion print("") könnt ihr zwischen den Anführungszeichen einen Text schreiben. Wir verwenden hier aber nur einzelne Zeichen. Mit vielen solchen Zeichen können wir vom Computer ein Bild "malen" lassen. Das nennt man "ASCII-Art". Tierbeispiele findest du hier: https://www.asciiart.eu/animals!
Viel Spass :D .

---
---
---
---
---
---
---
---

# 1. Python Programmierumgebung offline auf deinem Computer

![python-logo@2x.png](/img/python-logo@2x.png "Python")

Installiere die offizielle Python Programmierumgebung auf deinem Computer!

```python
print("Hello world!")
```

# Anleitung der Aufgabe

1. Gehe auf https://www.python.org/downloads/ und klicke auf den Download für dein Betriebssystem.
2. Öffne aus den heruntergeladenen Programmen "IDLE".
3. Im Menu klickst du auf "File" und dann "New File". Hier schreibst du deinen Code! Probiere es zuerst einmal mit dem Code oben aus.

> Um den Code auszuführen klickst du im Menu auf "Run" und dann "Run Module". Du musst zuerst diesen Code speichern und ihm einen Namen geben. Es ist eine gute Idee dein erstes Programm "HelloWorld" zu nennen :) .
