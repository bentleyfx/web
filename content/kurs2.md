---
title: "Blender 2.8"
date: 2019-06-26T15:52:23+02:00
draft: false
---
[kilo.jpg]: ../kilo.jpg "Key In Living Opensource"

Basic 3D modellieren mit Blender 2.8 

![kilo.jpg][kilo.jpg]

Die Kurse gehen jeweils 2h. Ein Kurs kostet CHF 300.- geteilt durch die 
Anzahl Teilnehmenden und ab 5 Anmeldungen CHF 60.- pro Person. 
Die [Anmeldung](mailto:felix.baenteli@protonmail.com) geschieht mit einer E-Mail an mich.

<br>
