---
title: "Kontakt"
date: 2020-07-02T16:13:44+02:00
showDate: false
---
Felix Bänteli, Künstler / S' Atelier Lit, Luzernerstrasse 127, 6014 Luzern

Ich freue mich auf Anfragen per [E-Mail](mailto:felix.baenteli@protonmail.com).
![](/img/MrFX.jpg)

# Ausbildung und beruflicher Werdegang

Seit 2018

Geschäftsführer der Firma [Actioncy GmbH](http://www.actioncy.ch)

2012 - 2019

Lehrtätigkeit als E-Gitarrist und Vermittlung von elektronischer Musik für eine ganzheitliche Musik-
pädagogik in Zug mit Simon Berz, Simon Britschgi, Florian Schneider und Lukas Weber bei [BADABUM](https://www.badabum.ch)
 
2012 - 2014	

Geschäftsführer des Vereins [SGMK](https://www.mechatronicart.ch), Schweizerische Gesellschaft für Mechatronische Kunst 

2009 - 2011	

Maschinentechnikstudium an der HSLU Technik & Architektur in Horw LU

2005 - 2008	

Bachelor-Studium Kunst & Vermittlung an der HSLU Design & Kunst in Luzern LU
		
2004 - 2005	

Vorkurs an der SfGB + B in Bern BE, für in eine Fachhochschule für Gestaltung 

2003 - 2004	

Kunstgeschichtsstudium an der Universität Freiburg FR

# Webseite

Diese Webseite wurde mit [HUGO](https://gohugo.io/) einem Static Webseiten Generator gebaut. Das Layout stammt von dem [Theme "Call me Sam!"](https://github.com/victoriadrake/hugo-theme-sam) von Victoria Drake. Die Markdown-Files werden über die Kommandozeile am Computer lokal geschrieben. Über git lade ich diese Files auf [Gitlab](https://gitlab.com/). Dank Gitlab-Pages übersetzt der Server mittels HUGO die Files in HTML und zeigt sie als Webseite an.

# Links

[Actioncy GmbH](https://actioncy.ch/)

[Videos von Bentley FX auf Vimeo](https://vimeo.com/user71901967)

[Webseite von der Künstlerin kaascat](https://kaascat.ch/)

### Erscheinungen und Erwähnungen

2018: [Erwähnung in der New York Times im Artikel von Jada Yuan](https://www.nytimes.com/2018/09/18/travel/lucerne-switzerland-mountains-views-lake.html)

2018: [Workshops über die SGMK](https://mechatronicart.ch/workshops)

2017: [Werkwanderung, Werkbund Zentralschweiz](http://werkwandern.ch/wp-content/uploads/2017/02/9_WERKWANDERUNG-PRINTVERSION1.pdf)

2017: [Werkwanderung, Artikel von Laura Livers auf zentralplus](https://www.zentralplus.ch/auf-chaotischer-strasse-mit-tueftlern-unterwegs-776505/)

2016: [Interview mit Dominik Landwehr im Band "Digital Kids"](https://www.digitalbrainstorming.ch/de/program/edc-schuber) 

2016: [Vortrag und PCB-Etching-Workshop am CAPTCHA-Designfestival](http://2016.captcha-mannheim.de/)

2016: [Eindrücke vom CAPTCHA-Designfestival](https://vimeo.com/205292647)

2015: [Im Hackspace vom LABOR Luzern](https://www.zentralplus.ch/sie-hacken-aber-keine-computer-719163/)

2014: [Boulderhalle BLOCKFELD Winterthur](https://blockfeld.ch/tag/kulturkopf-2013/)

### Links zur Person

2015: [Piezoinstallation, Kunsthoch Luzern](https://kalender.null41.ch/content/felix-baenteli)

2014: [Interview mit Sorino, Laufbahnberatung](http://sorino.ch/2014/08/11/mit-lotkolben-musik-machen/)

2014: [Portrait von SRF](http://www.srf.ch/news/regional/sommerserie/felix-baenteli-der-tueftelnde-kuenstler)

2013: [Kulturkopf Luzern, PDF](https://www.null41.ch/sites/default/files/magazine_archive/041_Kulturmagazin_Januar_14_GzD_2.pdf)

2013: [Kulturkopf Luzern](https://www.null41.ch/blog/wer-sind-die-kulturkoepfe-2014)

### Archiv Projekte

2018: [Vorstellung alter Hackspace LABOR Luzern von "Bildungsdesign"](https://www.youtube.com/watch?v=e_D2XsFMIbs&feature=youtu.bev)

2013: [Alte Files vom CreativeGroove auf Soundcloud](https://soundcloud.com/creativegroove)

2012: [SGMK MicroNoise Workshop mit BomboBombo in Zug](https://www.youtube.com/watch?v=-sqntDPpgxs)

2012: [Alle Dateien und Files zum WatchOut!-DIY-Mini-Synthesizer](https://www.thingiverse.com/thing:37478)

2012: [Demo Synthie-Watch-Sound "WatchOut!" Version Roman Jurt](https://www.youtube.com/watch?v=k94ccUMakCU)

2012: [Interview Radio 3Fach, Kulturbüro Luzern, PiezoLötsession](https://soundcloud.com/radio3fach/felix-b-nteli-ber-die-piezo-l)

2012: [Archiv CreativeGroove auf dem alten LABOR Luzern Blog](http://laborluzern.blogspot.ch/p/creative-groove.html)

### Mehr Kunst

2021-2022: [Kunstarbeit mit kaascat für Hackteria.org: Bärtierchen-Schutzanzug in der Ausstellung SUPER - Die zweite Schöpfung im Museum für Kommunikation Bern](https://my.matterport.com/show/?m=PJBPTGG1Gqz&sr=-2.92%2C-.63&ss=182&fbclid=IwAR3wj3CefD_Y4m1mhsZySIdtVFPzDl6qyf6HjhQ3Q8GI6Jgb0zlmypc4ORY)

2018: [Soundtrack zu Illustrationen von kaascat](https://www.youtube.com/watch?v=0YPsYy7xDvY)

2015: [BADABUM MONSTERGROOVE 2015](https://www.youtube.com/watch?v=awkPa86BLMk)

2014: [BADABUM MONSTERGROOVE 2014](https://vimeo.com/98626777)

2013: [Crealab Weihnachten](https://vimeo.com/81944048)

2012: [Music Session @ Homemade, Vico Morcote](https://vimeo.com/47972440) 
