![pong_cabbig-web.jpg](/img/pong_cabbig-web.jpg "Pong")

# Pong 

# Pong alles nur gezeichnet

```python
from turtle import *
import turtle

hoch = 10
breit = 30
gross = [hoch,breit]*2
play = turtle.Turtle()
ball = turtle.Turtle()
screen = turtle.Screen()
screen.tracer(0)

ball.shape("circle")
ball.pu()
ball.goto(0,150)
ball.pd()

play.ht()
play.pu()
play.goto(0,-100)
play.pd()

# draw a square
def schlag():
  play.clear()
  play.fillcolor("black")
  play.begin_fill()
  for i in gross:
    play.rt(90)
    play.fd(i)
  play.end_fill()
  screen.update()
    
schlag()

```


# Pong mit Schläger bewegt

```python
from turtle import *

hoch = 10
breit = 30
gross = [hoch,breit]*2
play = turtle.Turtle()
ball = turtle.Turtle()
screen = turtle.Screen()
screen.tracer(0)

ball.shape("circle")
ball.pu()
ball.goto(0,150)
ball.pd()

play.ht()
play.pu()
play.goto(0,-100)
play.pd()

# draw a square
def schlag():
  play.clear()
  play.fillcolor("black")
  play.begin_fill()
  for i in gross:
    play.rt(90)
    play.fd(i)
  play.end_fill()
  screen.update()
    
schlag()

def mover():
  play.pu()
  play.fd(10)
  play.pd()
  schlag()

def movel():
  play.pu()
  play.fd(-10)
  play.pd()
  schlag()

# move schlag
screen.onkey(mover, "Right")
screen.onkey(movel, "Left")
screen.listen()
screen.mainloop()

```

# Pong mit Schläger und Ball bewegt

```python
# Turtle Bibliothek importieren
from turtle import *

# Grösse Schläger definieren
hoch = 10
breit = 30
gross = [hoch,breit]*2

# Schläger und Ball als eigene Schildkröten 
play = turtle.Turtle()
ball = turtle.Turtle()

# Leinwand definieren 
leinwand = turtle.Screen()

# Bitte alles auf einmal zeichnen
leinwand.tracer(0)

# Ball zeichnen
ball.shape("circle")
ball.pu()
ball.goto(0,200)

# Ball vertikal bewegen (max. -5)
ball.dy = -5

# An Schlägerposition gehen
play.ht()
play.pu()
play.goto(0+breit/2,-100)
play.pd()

# Schläger definieren in einer Funktion
def schlag():
  play.clear()
  play.fillcolor("black")
  play.begin_fill()
  for i in gross:
    play.rt(90)
    play.fd(i)
  play.end_fill()
  leinwand.update()

# Schläger an der aktuellen Position zeichnen
schlag()

# Bewegung des Schlägers nach Rechts definieren
def bewegung_r():
  play.pu()
  play.fd(10)
  play.pd()
  schlag()

# Bewegung des Schlägers nach Links definieren
def bewegung_l():
  play.pu()
  play.fd(-10)
  play.pd()
  schlag()

# Tastaturbefehle dazufügen
leinwand.onkey(bewegung_r, "Right")
leinwand.onkey(bewegung_l, "Left")
leinwand.listen()
leinwand.mainloop()

# Eine Schleife, die den Ball in Bewegung bringt
while ball.ycor() > -190:

  # ball.setx(ball.xcor()+ball.dx)
  ball.sety(ball.ycor()+ball.dy)

  # Eine Überprüfung der Position
  if (ball.ycor() == play.ycor()+10 and play.xcor() >= -5 and play.xcor() <= breit) or ball.ycor() >= 200:
    ball.dy *= -1

  # Wieder den Bildschirm aktualisieren
  leinwand.update()

```    
