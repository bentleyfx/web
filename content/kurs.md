---
title: "Computer- und Softwarekurse"
date: 2019-06-26T15:52:23+02:00
draft: false
---

![Ich gebe euch den Schlüssel zum Leben mit offenen Quellen!](/img/GenderBlender_OnlineSessions_SonmasMrFX.jpg "Key In Living Opensource")

Ich gebe mein Grundwissen über verschiedene Open Source Programme weiter. 
Dieses Angebot sind Einsteiger-Software-Kurse, die vor allem auf Kulturschaffende 
und solche, die es noch werden wollen, ausgerichtet sind. 

- Funktionen in
 [**LibreOffice Calc**](/kurs1/) 

- Basic 3D mit
 [**Blender**](/kurs2/) 

- Livecoding in
 [**SonicPi**](/kurs3/) 

Die Kurse gehen jeweils 2h. Ein Kurs kostet CHF 300.- geteilt durch die 
Anzahl Teilnehmenden und ab 5 Anmeldungen CHF 60.- pro Person. 
Die [Anmeldung](mailto:felix.baenteli@protonmail.com) geschieht mit einer E-Mail an mich.

# Weitere Kursangebote
Mit der Workshopagentur [Actioncy GmbH](https://www.actioncy.ch) arbeite ich an vielen weiteren mechatronischen 
Kunst-Workshops rund um den Themenkreis von freier Soft- und Hardware.

![](/img/FelixBaenteli_ActioncyGmbH.jpeg)

Für Gross und Klein bieten wir in der ganzen Schweiz Workshops und Teamevents zu Sound, Visualz und Robotik an. 

[Kontaktieren](mailto:felix.baenteli@actioncy.ch)  Sie uns!


# DIY Workshops
Für verschiedene mechatronische Kunst-Lötworkshops habe ich elektronische Leiterplatinen / PCBs gezeichnet. 
[![Hier gelangen Sie zur PCB-Galerie.](/pcbs/WizardsChamber.jpg "Klicken Sie hier!")](/pcbs)
Die elektronischen Schaltungen sind aus der Open Source Community und die Entwicklung ist auf dem [Wiki](http://wiki.sgmk-ssam.ch/wiki/Main_Page) der 
Schweizerischen Gesellschaft für Mechatronische Kunst – [SGMK](https://mechatronicart.ch) dokumentiert.

Die Plattform [Kitspace](https://www.kitspace.org) macht möglich, dass man für Workshops einfach die Menge Bausätze 
bestellen kann, die man für den Lötunterricht braucht. Ich arbeite daran meine Designs da auch raufzuladen.

