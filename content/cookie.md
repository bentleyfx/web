![cookie-clicker.jpg](/img/cookie-clicker.jpg "cookie")

# Cookie online


```python
# Alles vom Turtle-Modul in Python aufrufen für unser Guetzli-Rezept
from turtle import *

# Ausstanzform und Werkzeuge für Guetzli bereit machen
cookie = Turtle()
blech = Screen()
cookie.shape("circle")
hefe = 55

# Vorbereiten, Teig machen und auswallen
def teig(hefe):
  blech.tracer(0)
  cookie.fillcolor("black")
  cookie.begin_fill()
  cookie.pu()
  cookie.rt(90)
  cookie.fd(hefe)
  cookie.rt(-90)
  cookie.circle(hefe,360)
  cookie.goto(0,0)
  cookie.end_fill()
  blech.update()

# Form ausstechen und auf Backblech legen
teig(hefe)

# Überlegen wie braten
def gross(brat):
  tracer(0)
  teig(brat)
  update()
  delay(30)
  cookie.clear()
  teig(hefe)
  Screen().update()
  cookie.clear()

# Timer ans richtige Ort stellen
zahl = Turtle()
zahl.ht()
zahl.pu()
zahl.goto(0,-200)
zaehl = 0

# In Pfanne Braten
def braten(x, y):
    global zaehl # Variable "count" aus dem Hauptprogramm in lokale "braten"-Programm holen
    zahl.clear()
    zaehl += 1
    zahl.write(zaehl, align="center", font=('IBM Plex Mono', 24, 'normal'))
    gross(70)
    
# Braten, wenn auf Guetzli geklickt wird    
cookie.onclick(braten, btn=1)

```









# Cookie einfacher in der IDLE


```python
# Turtle-Modul in Python aufrufen 
from turtle import *

# Guetzli zeichnen
cookie = Turtle()
cookie.shape("circle")
cookie.shapesize(10,10)

# Schildkröte als Zähler definieren und an nötige Position setzen
counter = Turtle()
counter.ht()
counter.pu()
counter.goto(0,-200)

# Variable als Zähler definieren
count = 0

# Programm definieren, dass bei einem Klick aufgerufen werden soll
def cook(x, y):
    global count # Globale Variable "count" aus dem Hauptprogramm in lokales "cook"-Programm holen
    counter.clear()
    count += 1
    counter.write(count, align="center", font=('IBM Plex Mono', 24, 'normal'))
    cookie.shapesize(9,9)
    delay(25)
    cookie.shapesize(10,10)

# Funktion "cook" aufrufen, wenn auf Guetzli geklickt wird    
cookie.onclick(cook, btn=1)
```
