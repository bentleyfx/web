# Design and Art

My creative work ranges from rough graphic design, crazy flyers, lunatic logos to experimental 3D renderings, weird objects and wild videos.

[![Screenshot of polygonal lion made with blender.org cc by mrfx 2018](/img/Loewe_Poly.png)](https://player.vimeo.com/video/909545362?)
Screenshot of polygonal lion made with blender.org //
Click on the picture to see the video // cc by mrfx 2019

# LoeweLovers

For the 150th anniversary of the (ambivalent) sculpture of a dying lion aka lionmonument in Lucerne, Switzerland we took part of a big pixel-wall-installation with the local tinkering- and hacking-association laborluzern.ch. "We" are kaascat.ch and me mrfx.ch. Our "pixel" was a Loewens-TV with an animated video from the part of the lion the pixel represented. 

{{< vimeo id="909545362?" title="LoeweLoversLucerne" >}}
