
# 1. Arduino Basics

Das [Arduino](https://www.arduino.cc/) verbindet über angeschlossene Sensoren die reale physische Welt mit der Welt des Virtuellen und steuert wiederrum im Code definiert Aktoren wie LEDs, Piezolautsprecher und Motoren. 

![Arduino.webp](/img/Arduino.webp "Arduino")

> Es gibt analoge Inputs, digitale Inputs und digitale Outputs. Mit [PWM](https://en.wikipedia.org/wiki/Pulse-width_modulation) kann man an den Ausgängen mit dem Zeichen "~" auch analoge Spannungen simulieren. 


# Anleitung der Aufgabe

1. Lade den Knob-Beispielcode (File/Examples/Servo/Knob) auf das Arduino.
2. Klicke im Code auf den Link im Kommentar und stecke den Servo und das Potentiometer wie im Bild ins Breadboard.
3. Ergänze den Code mit der Serial-Kommunikation um am Serial Monitor die Daten zu sehen.

```arduino
void setup() {
Serial.begin(9600)
}

void loop() {
Serial.println(val)
}
```
________________________________________________________________________________

# 2. Arduino Advanced

Das Potentiometer aus der letzten Aufgabe macht einen Spannungsteiler. Das ist notwendig damit das Arduino eine Änderung der Spannung messen und in Werte bis zu 10bit umwandeln kann.

![SERVO_Steckplatine.png](/img/SERVO_Steckplatine.png "Servo Steckplatine")

# Anleitung der Aufgabe

1. Stecke die Verbindungen wie oben aufgezeigt auf dem Breadboard.
2. Lade das Programm “Knob” aus den Examples (File/Examples/Servo/Knob) mit der ergänzten Serial-Kommunikation auf das Arduino.
3. Verändere den am Computer angezeiten Sensor-Wert passend zu den Werten die der LDR liefert mit der Funktion “map” auf Zeile 23.

> Die Werte dürfen 10bit nicht überschreiten (Umrechnung von bits ins binäre System: [Arndt Bruenner](https://www.arndt-bruenner.de/mathe/scripts/Zahlensysteme.htm)).
Genauere Erklärungen zu “map” findest du in der Code-Referenz mit dem Cursor auf “map” und dann Rechtsklick.

________________________________________________________________________________

# 3. Eigener Sensor

Für diese Aufgabe sucht ihr euch einen eigenen Sensor. Zum Beispiel den Ultraschallsensor HC-SR05 (siehe Links unten). Klappt die Distanzmessung?

________________________________________________________________________________


# 4. Motoren und Motorshields

Motoren brauchen viel Strom. Das Arduino kann ausser direkt bei den "5V" und beim "Vin" nur 50mA aus den Pins liefern. Um mehr Strom zu bekommen nutzen wir einen zusätzlichen Chip. Entweder kommt er als MOSFET, L293D oder auf einem Shield. Der A4988-Motortreiberchip ist speziell für Steppermotoren und wird für das CNC-Shield im Motor Extended Kit benötigt. Siehe unten die Links.  

________________________________________________________________________________

# Links

[wokwi: Beispiel Knob](https://wokwi.com/projects/340367397829476948)

[Projekt Lab2Go: HSLU DI](http://lab2go.enterpriselab.ch/)

[Quad Motor Driver Shield V1.0](https://wiki.dfrobot.com/Quad_Motor_Driver_Shield_for_Arduino_SKU_DRI0039)

[HC-SR05 Code](https://savecode.net/code/arduino/hc+sr05+arduino)

[HC-SR05 Schaltplan](https://funduino.de/nr-10-entfernung-messen) 

[CNC-Shield: Videos Gordan](https://tube.switch.ch/channels/Dcqw1ga3NL)

[A4988 Stepper Driver Erklärung](https://howtomechatronics.com/tutorials/arduino/how-to-control-stepper-motor-with-a4988-driver-and-arduino/)

